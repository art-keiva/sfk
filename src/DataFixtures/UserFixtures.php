<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Service\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture {

    public const ENTITY_REFERENCE = 'user';

    private $fixtures;
    private $passwordEncoder;

    public function __construct(DataFixtures $fixtures, UserPasswordEncoderInterface $passwordEncoder) {
        $this->fixtures = $fixtures;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager) {
        $this->loadEntities($manager);
    }

    private function loadEntities(ObjectManager $manager): void {
        $i = 1;
        foreach ($this->getEntityData() as [$fullname, $username, $password, $email, $roles]) {
            $entity = new User();
            $entity->setFullName($fullname);
            $entity->setUsername($username);
            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $password));
            $entity->setEmail($email);
            $entity->setRoles($roles);

            $manager->persist($entity);

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function getEntityData(): array {
        return [
            // $userData = [$fullname, $username, $password, $email, $roles];
            ['Keiva', 'keiva', 'A1Ek45A0K', 'malibu16rus@gmail.com', ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN']],
            ['Влад', 'vlad', 'uEroZs7hM', 'alf-site@mail.ru', ['ROLE_ADMIN']],
            ['Ринат', 'rinatuss', 'rinatuss', 'rinatuss@yandex.ru', ['ROLE_USER']],
        ];
    }

}
