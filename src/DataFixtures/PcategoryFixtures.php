<?php

namespace App\DataFixtures;

use App\Entity\Pcategory;
use App\Utils\Slugger;
use App\Service\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PcategoryFixtures extends Fixture
{
    public const ENTITY_REFERENCE = 'pcategory';
    private $slugger;
    private $fixtures;
    private $entities;

    public function __construct(Slugger $slugger, DataFixtures $fixtures)
    {
        $this->slugger = $slugger;
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
        $this->loadParents($manager);
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getShortPhrases() as $i => $title) {
            $entity = new Pcategory();
            $entity->setTitle( $title );
            $entity->setDescription( $this->fixtures->getRandomParagraphs(random_int(4, 10)) );
            $entity->setMetaTitle( $title );
            $entity->setMetaDescription('');
            $entity->setMetaKeyword('');
            $entity->setImage( $this->fixtures->getRandomImage() );
            $entity->setSlug( $title );
            $entity->setSort( $i );
            $entity->setEnabled(true);

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadParents(ObjectManager $manager): void
    {
        $categories = [];

        $i = 1;
        while ($this->hasReference($entityReference = self::ENTITY_REFERENCE . '_' . $i++)) {
            $categories[] = $this->getReference($entityReference);
        }

        $i = 1;
        foreach ($categories as $key => $entity) {
            if ($i % 2) {
                $tempArray = $categories;
                
                unset($tempArray[$key]);

                $randomEntityKey = array_rand($tempArray);

                $entity->setParent($categories[$randomEntityKey]);

                $manager->persist($entity);
            }
            
            $i++;
        }

        $manager->flush();
    }
}
