<?php

namespace App\DataFixtures;

use App\Entity\Page;
use App\Entity\Slug;
use App\Utils\Slugger;
use App\Service\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PageFixtures extends Fixture
{
    public const ENTITY_REFERENCE = 'page';
    private $slugger;
    private $fixtures;

    public function __construct(Slugger $slugger, DataFixtures $fixtures)
    {
        $this->slugger = $slugger;
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
        $this->loadParents($manager);
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->getEntityData() as [$title, $controller, $parent, $enabled, $top, $bottom, $slug, $image, $description]) {
            $entity = new Page();
            $entity->setTitle($title);
            $entity->setDescription( $description );
            $entity->setMetaTitle($title);
            $entity->setMetaDescription('');
            $entity->setMetaKeyword('');
            $entity->setController($controller);
            // $entity->setParent($controller);
            $entity->setImage($image);
            $entity->setSort($i);
            $entity->setEnabled($enabled);
            $entity->setTop($top);
            $entity->setBottom($bottom);

            if ($slug || $i == 1) {
                $entity->setSlug($slug);
            }

            $manager->persist($entity);

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadParents(ObjectManager $manager): void
    {
        $pages = [];
        $i = 1;

        while ($this->hasReference($entityReference = self::ENTITY_REFERENCE . '_' . $i)) {
            $pages[] = $this->getReference($entityReference);
            $i++;
        }

        foreach ($this->getEntityData() as [$title, $controller, $parent, $enabled, $top, $bottom, $slug, $image, $description]) {
            
            if (!empty($parent)) {

                foreach ($pages as $key => $entity) {

                    if ($entity->getTitle() === $title) {

                        foreach ($pages as $key2 => $entity2) {
                            $parentEntity = null;

                            if ($entity2->getController() === $parent) {
                                $parentEntity = $entity2;
                                break;
                            }
                        }

                        $entity->setParent($parentEntity);
                        $manager->persist($entity);
                    }
                }
            }
        }

        $manager->flush();
    }

    private function getEntityData(): array
    {
        return [
// [$title,             $controller,    $parent,    $enabled, $top,   $bottom, $slug,      $image,      $description];
['Главная'              , ''            , ''        , true    , true  , true   , ''        , ''        , ''],
['О нас'                , 'about'       , ''        , true    , true  , true   , ''        , $this->fixtures->getRandomImage(), $this->fixtures->getRandomParagraph(random_int(128, 512))],
['Услуги'               , 'service'     , ''        , true    , true  , true   , ''        , ''        , ''],
['Каталог'              , 'catalog'     , ''        , true    , true  , true   , ''        , ''        , ''],
['Акции'                , 'action'      , ''        , true    , true  , true   , ''        , ''        , $this->fixtures->getRandomParagraph()],
['Специалисты'          , 'specialist'  , 'about'   , true    , true  , false  , ''        , ''        , ''],
['Прайс'                , ''            , 'about'   , true    , true  , true   , ''        , ''        , $this->fixtures->getPriceContent()],
['Документы'            , ''            , 'about'   , true    , true  , true   , ''        , ''        , $this->fixtures->getRandomParagraph()],
['Новости'              , 'blog'        , ''        , true    , true  , true   , ''        , ''        , ''],
['Контакты'             , 'contact'     , ''        , true    , true  , true   , ''        , ''        , ''],
['Вопросы'              , 'faq'         , 'about'   , true    , true  , false  , ''        , ''        , ''],
['Галерея'              , 'gallery'     , 'about'   , true    , true  , false  , ''        , ''        , ''],
['Отзывы'               , 'testimonial' , 'about'   , true    , true  , false  , ''        , ''        , ''],
['Цены'                 , 'price'       , 'about'   , true    , true  , false  , ''        , ''        , ''],
['Условия соглашения'   , 'terms'       , ''        , true    , false , true   , ''        , ''        , ''],
        ];
    }
}
