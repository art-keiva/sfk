<?php

namespace App\DataFixtures;

use App\Entity\Info;
use App\Service\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class InfoFixtures extends Fixture
{
    public const ENTITY_REFERENCE = 'info';
    private $fixtures;
    private $entities;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->getEntityData() as [$key, $name, $value]) {
            $entity = new Info();
            $entity->setKey( $key );
            $entity->setName( $name );
            $entity->setValue( $value );

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function getEntityData(): array
    {
        return [
// [$key,               $name,                                  $value];
['project'                      , 'sitename'                            , 'Название компании'],
['project'                      , 'email'                               , 'info@example.com'],
['project'                      , 'icon'                                , 'favicon.png'],
['header'                       , 'logo'                                , 'your-logo.png'],
['header'                       , 'address'                             , 'г. Название города, ул. Название улицы, д. 10А'],
['header'                       , 'phone_1'                             , '495 356-95-27'],
['header'                       , 'phone_2'                             , '495 356-95-28'],
['header'                       , 'phone_3'                             , ''],
['header'                       , 'email'                               , 'company@example.com'],
['header'                       , 'open'                                , 'Пн - Сб: 7:00 - 17:00'],
['social'                       , 'inst'                                , 'https://www.instagram.com'],
['social'                       , 'vk'                                  , 'https://www.vk.com'],
['footer'                       , 'logo'                                , 'your-logo.png'],
['footer'                       , 'comment'                             , $this->fixtures->getRandomParagraph(120, 'class="small"')],
['open'                         , 'open_1'                              , '9.00 - 20.00'],
['open'                         , 'open_2'                              , '9.00 - 20.00'],
['open'                         , 'open_3'                              , '9.00 - 20.00'],
['open'                         , 'open_4'                              , '9.00 - 20.00'],
['open'                         , 'open_5'                              , '9.00 - 20.00'],
['open'                         , 'open_6'                              , '9.00 - 20.00'],
['open'                         , 'open_7'                              , '9.00 - 20.00'],
['size'                         , 'width_default'                       , '320'],
['size'                         , 'height_default'                      , '280'],
['size'                         , 'mode_default'                        , '1'],
['size'                         , 'width_product'                       , '220'],
['size'                         , 'height_product'                      , '200'],
['size'                         , 'mode_product'                        , '1'],
['size'                         , 'width_product_show'                  , '320'],
['size'                         , 'height_product_show'                 , '262'],
['size'                         , 'mode_product_show'                   , '1'],
['size'                         , 'width_product_popup'                 , '798'],
['size'                         , 'height_product_popup'                , '726'],
['size'                         , 'mode_product_popup'                  , '0'],
['size'                         , 'width_pcategory'                     , '220'],
['size'                         , 'height_pcategory'                    , '200'],
['size'                         , 'mode_pcategory'                      , '1'],
['size'                         , 'width_pcategory_show'                , '320'],
['size'                         , 'height_pcategory_show'               , '262'],
['size'                         , 'mode_pcategory_show'                 , '1'],
['size'                         , 'width_service'                       , '350'],
['size'                         , 'height_service'                      , '240'],
['size'                         , 'mode_service'                        , '1'],
['size'                         , 'width_service_show'                  , '286'],
['size'                         , 'height_service_show'                 , '260'],
['size'                         , 'mode_service_show'                   , '1'],
['size'                         , 'width_service_popup'                 , '798'],
['size'                         , 'height_service_popup'                , '726'],
['size'                         , 'mode_service_popup'                  , '0'],
['size'                         , 'width_scategory'                     , '255'],
['size'                         , 'height_scategory'                    , '202'],
['size'                         , 'mode_scategory'                      , '1'],
['size'                         , 'width_scategory_show'                , '286'],
['size'                         , 'height_scategory_show'               , '260'],
['size'                         , 'mode_scategory_show'                 , '1'],
['size'                         , 'width_action'                        , '750'],
['size'                         , 'height_action'                       , '530'],
['size'                         , 'mode_action'                         , '1'],
['size'                         , 'width_action_show'                   , '750'],
['size'                         , 'height_action_show'                  , '530'],
['size'                         , 'mode_action_show'                    , '1'],
['size'                         , 'width_action_popup'                  , '750'],
['size'                         , 'height_action_popup'                 , '530'],
['size'                         , 'mode_action_popup'                   , '0'],
['size'                         , 'width_specialist'                    , '274'],
['size'                         , 'height_specialist'                   , '368'],
['size'                         , 'mode_specialist'                     , '1'],
['size'                         , 'width_specialist_show'               , '360'],
['size'                         , 'height_specialist_show'              , '480'],
['size'                         , 'mode_specialist_show'                , '1'],
['size'                         , 'width_specialist_popup'              , '360'],
['size'                         , 'height_specialist_popup'             , '480'],
['size'                         , 'mode_specialist_popup'               , '0'],
['size'                         , 'width_gallery'                       , '267'],
['size'                         , 'height_gallery'                      , '200'],
['size'                         , 'mode_gallery'                        , '1'],
['size'                         , 'width_gallery_popup'                 , '798'],
['size'                         , 'height_gallery_popup'                , '726'],
['size'                         , 'mode_gallery_popup'                  , '0'],
['size'                         , 'width_blog'                          , '850'],
['size'                         , 'height_blog'                         , '410'],
['size'                         , 'mode_blog'                           , '1'],
['size'                         , 'width_blog_show'                     , '850'],
['size'                         , 'height_blog_show'                    , '410'],
['size'                         , 'mode_blog_show'                      , '1'],
['size'                         , 'width_blog_popup'                    , '1000'],
['size'                         , 'height_blog_popup'                   , '486'],
['size'                         , 'mode_blog_popup'                     , '0'],
['size'                         , 'width_bcategory'                     , '850'],
['size'                         , 'height_bcategory'                    , '410'],
['size'                         , 'mode_bcategory'                      , '1'],
['size'                         , 'width_bcategory_show'                , '850'],
['size'                         , 'height_bcategory_show'               , '410'],
['size'                         , 'mode_bcategory_show'                 , '1'],
['size'                         , 'width_testimonial'                   , '340'],
['size'                         , 'height_testimonial'                  , '240'],
['size'                         , 'mode_testimonial'                    , '1'],
['size'                         , 'width_testimonial_popup'             , '850'],
['size'                         , 'height_testimonial_popup'            , '600'],
['size'                         , 'mode_testimonial_popup'              , '0'],
['size'                         , 'width_page'                          , '400'],
['size'                         , 'height_page'                         , '280'],
['size'                         , 'mode_page'                           , '0'],
['size'                         , 'width_slider'                        , '1920'],
['size'                         , 'height_slider'                       , '528'],
['size'                         , 'mode_slider'                         , '1'],
['size'                         , 'width_banner'                        , '350'],
['size'                         , 'height_banner'                       , '164'],
['size'                         , 'mode_banner'                         , '1'],
['size'                         , 'width_counter'                       , '256'],
['size'                         , 'height_counter'                      , '256'],
['size'                         , 'mode_counter'                        , '1'],
['size'                         , 'width_advantage'                     , '360'],
['size'                         , 'height_advantage'                    , '270'],
['size'                         , 'mode_advantage'                      , '1'],
['size'                         , 'width_infoblock'                     , '340'],
['size'                         , 'height_infoblock'                    , '274'],
['size'                         , 'mode_infoblock'                      , '1'],
['size'                         , 'width_textblock'                     , '262'],
['size'                         , 'height_textblock'                    , '196'],
['size'                         , 'mode_textblock'                      , '1'],
['size'                         , 'width_contact'                       , '633'],
['size'                         , 'height_contact'                      , '400'],
['size'                         , 'mode_contact'                        , '1'],
['size'                         , 'width_contact_popup'                 , '960'],
['size'                         , 'height_contact_popup'                , '578'],
['size'                         , 'mode_contact_popup'                  , '0'],
['size'                         , 'width_section_background'            , '1920'],
['size'                         , 'height_section_background'           , '440'],
['size'                         , 'mode_section_background'             , '1'],
// [''                             , ''                                    , ''],
// [''                             , ''                                    , ''],
// [''                             , ''                                    , ''],
// [''                             , ''                                    , ''],
// [''                             , ''                                    , ''],
// [''                             , ''                                    , ''],
        ];
    }
}
