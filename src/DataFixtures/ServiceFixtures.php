<?php

namespace App\DataFixtures;

use App\Entity\Scategory;
use App\Entity\Service;
use App\Utils\Slugger;
use App\Service\DataFixtures;
use App\DataFixtures\ScategoryFixtures as Categories;
use App\DataFixtures\ImageFixtures as Images;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ServiceFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'service';
    private $slugger;
    private $fixtures;
    private $entities;

    public function __construct(Slugger $slugger, DataFixtures $fixtures)
    {
        $this->slugger = $slugger;
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
        $this->loadEntityCategory($manager);
        $this->loadEntityImages($manager);
    }

    public function getDependencies()
    {
        return array(
            Categories::class,
            Images::class,
        );
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getPhrases() as $i => $title) {
            $entity = new Service();
            $entity->setTitle( $title );
            $entity->setSummary( $this->fixtures->getRandomText(128) );
            $entity->setDescription( $this->fixtures->getRandomParagraphs(random_int(4, 10)) );
            $entity->setMetaTitle( $title );
            $entity->setMetaDescription('');
            $entity->setMetaKeyword('');
            $entity->setImage( $this->fixtures->getRandomImage(self::ENTITY_REFERENCE) );
            $entity->setPrice( random_int(100, 9999) );
            $entity->setSlug( $title );
            $entity->setSort( $i );
            $entity->setEnabled(true);
            $entity->setSpecial($i % 2);

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadEntityCategory(ObjectManager $manager): void
    {
        $categories = [];

        $i = 1;
        while ($this->hasReference($entityReference = Categories::ENTITY_REFERENCE . '_' . $i++)) {
            $category = $this->getReference($entityReference);
            if (null !== $category->getParent()) {
                $categories[] = $this->getReference($entityReference);
            }
        }

        $entities = [];

        $i = 1;
        while ($this->hasReference($entityReference = self::ENTITY_REFERENCE . '_' . $i++)) {
            // TODO: fix same in other entities
            $entities[] = $this->getReference($entityReference);
        }

        if ($categories) {
            foreach ($entities as $key => $entity) {
                $randomEntityKey = array_rand($categories);
                $entity->addScategory($categories[$randomEntityKey]);
                $entity->addScategory($categories[0]); // and add all service into first category for tests

                $manager->persist($entity);
            }
        }

        $manager->flush();
    }

    private function loadEntityImages(ObjectManager $manager): void
    {
        $images = [];
        $i = 1;

        while ($this->hasReference($entityReference = Images::ENTITY_REFERENCE . '_' . $i)) {
            $images[] = $this->getReference($entityReference);
            $i++;
        }

        if ($images) {
            foreach ($this->entities as $key => $entity) {
                // $randomEntityKey = array_rand($images);
                foreach ($images as $key2 => $entity2) {
                    // supposed 11 images in db and we split them between service, product, specialist entitites
                    if ($key2 <= 2) {
                        $entity->addImage($entity2); // and add all images into first entity for tests
                    }
                }

                $manager->persist($entity);

                break;
            }
        }

        $manager->flush();
    }
}
