<?php

namespace App\DataFixtures;

use App\Entity\Gallery;
use App\Service\DataFixtures;
use App\DataFixtures\GtagFixtures as Categories;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class GalleryFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'gallery';
    private $fixtures;
    private $entities;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
        $this->loadEntityCategory($manager);
    }

    public function getDependencies()
    {
        return array(
            Categories::class,
        );
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getPhrases() as $i => $title) {
            $entity = new Gallery();
            $entity->setTitle( $title );
            $entity->setImage( $this->fixtures->getRandomImage(self::ENTITY_REFERENCE) );
            $entity->setSort( $i );
            $entity->setEnabled(true);
            $entity->setSpecial($i % 2);

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadEntityCategory(ObjectManager $manager): void
    {
        $categories = [];
        $i = 1;

        while ($this->hasReference($entityReference = Categories::ENTITY_REFERENCE . '_' . $i)) {
            $categories[] = $this->getReference($entityReference);
            $i++;
        }

        if ($categories) {
            foreach ($this->entities as $key => $entity) {
                $randomEntityKey = array_rand($categories);
                $entity->setGtag($categories[$randomEntityKey]);

                $manager->persist($entity);
            }
        }

        $manager->flush();
    }
}
