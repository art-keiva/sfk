<?php

namespace App\DataFixtures;

use App\Entity\Gtag;
use App\Service\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class GtagFixtures extends Fixture
{
    public const ENTITY_REFERENCE = 'gtag';
    private $fixtures;
    private $entities;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
    }

    private function loadEntities(ObjectManager $manager): void
    {
        foreach ($this->fixtures->getTags() as $i => $title) {
            $entity = new Gtag();
            $entity->setTitle( ucfirst($title) );
            $entity->setSort( $i );
            
            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            if ($i >= 4) {
                break;
            }
        }

        $manager->flush();
    }
}
