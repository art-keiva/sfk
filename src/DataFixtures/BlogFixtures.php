<?php

namespace App\DataFixtures;

use App\Entity\Bcategory;
use App\Entity\Blog;
use App\Utils\Slugger;
use App\Service\DataFixtures;
use App\DataFixtures\BcategoryFixtures as Categories;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class BlogFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'blog';
    private $slugger;
    private $fixtures;
    private $entities;

    public function __construct(Slugger $slugger, DataFixtures $fixtures)
    {
        $this->slugger = $slugger;
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
        $this->loadEntityCategory($manager);
    }

    public function getDependencies()
    {
        return array(
            Categories::class,
        );
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getPhrases() as $i => $title) {
            $entity = new Blog();
            $entity->setTitle( $title );
            $entity->setSummary( $this->fixtures->getRandomText(120) );
            $entity->setDescription( $this->fixtures->getRandomParagraphs(random_int(4, 10)) );
            $entity->setMetaTitle( $title );
            $entity->setMetaDescription('');
            $entity->setMetaKeyword('');
            $entity->setImage( $this->fixtures->getRandomImage('blog') );
            $entity->setSlug( $title );
            $entity->setSort( $i );
            $entity->setEnabled(true);
            $entity->setSpecial( ($i < 3) ? true : false );

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadEntityCategory(ObjectManager $manager): void
    {
        $categories = [];
        $i = 1;

        while ($this->hasReference($entityReference = Categories::ENTITY_REFERENCE . '_' . $i)) {
            $category = $this->getReference($entityReference);
            // if (null !== $category->getParent()) {
                $categories[] = $this->getReference($entityReference);
            // }
            $i++;
        }

        if ($categories) {
            foreach ($this->entities as $key => $entity) {
                $randomEntityKey = array_rand($categories);
                $entity->setBcategory($categories[$randomEntityKey]);

                $manager->persist($entity);
            }
        }

        $manager->flush();
    }
}
