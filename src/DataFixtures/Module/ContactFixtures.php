<?php

namespace App\DataFixtures\Module;

use App\Entity\Contact;
use App\Service\DataFixtures;
use App\DataFixtures\Module\ModuleFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ContactFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'contact';
    private $fixtures;
    private $modules;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {

        $i = 1;
        while ($this->hasReference($entityReference = ModuleFixtures::ENTITY_REFERENCE . '_' . $i)) {
            $this->modules[] = $this->getReference($entityReference);
            $i++;
        }

        $this->loadEntityContact($manager);
    }

    public function getDependencies()
    {
        return array(
            ModuleFixtures::class,
        );
    }

    private function loadEntityContact(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->getEntityData() as [$title, $address, $phone1, $phone2, $phone3, $email, $coords, $image]) {
            $entity = new Contact();
            $entity->setTitle( $title );
            $entity->setAddress( $address );
            $entity->setPhone1( $phone1 );
            $entity->setPhone2( $phone2 );
            $entity->setPhone3( $phone3 );
            $entity->setEmail( $email );
            $entity->setCoords( $coords );
            $entity->setImage($image);
            $entity->setSort($i);

            foreach ($this->modules as $key2 => $module) {
                if ($module->getName() === 'contact' && $module->getSort() === 1) {
                    $entity->setModule($module);
                }
            }

            $manager->persist($entity);

            $i++;
        }

        $manager->flush();
    }

    private function getEntityData(): array
    {
        return [
            // $data = [$title, $address, $phone1, $phone2, $phone3, $email, $coords, $image];
            [
                'Казань, Оптовый склад',
                'Моторная улица, 31А, Казань, Республика Татарстан, Россия, 420105',
                '495 356-24-11',
                '495 356-24-12',
                '495 356-24-13',
                'company1@gmail.com',
                '55.768358, 49.162266',
                $this->fixtures->getImages('contact')[0],
            ],
            [
                'ООО "Боровецк"',
                'улица Гагарина, 14, Казань, Республика Татарстан, Россия, 420057',
                '495 356-24-21',
                '495 356-24-22',
                '',
                'company2@gmail.com',
                '55.838862, 49.093570',
                $this->fixtures->getImages('contact')[1],
            ],
            [
                'Магазин «Северный»',
                'Большая Покровская улица, 32, Нижний Новгород, Россия',
                '495 356-24-31',
                '',
                '',
                'company3@gmail.com',
                '56.317941, 43.996760',
                $this->fixtures->getImages('contact')[2],
            ],
        ];
    }
}
