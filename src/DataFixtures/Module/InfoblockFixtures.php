<?php

namespace App\DataFixtures\Module;

use App\Entity\Infoblock;
use App\Service\DataFixtures;
use App\DataFixtures\Module\ModuleFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class InfoblockFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'infoblock';
    private $fixtures;
    private $modules;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {

        $i = 1;
        while ($this->hasReference($entityReference = ModuleFixtures::ENTITY_REFERENCE . '_' . $i)) {
            $this->modules[] = $this->getReference($entityReference);
            $i++;
        }

        $this->loadEntityBanner($manager);
        $this->loadEntityAdvantage($manager);
        $this->loadEntityCounter($manager);
        $this->loadEntityGallery($manager);
    }

    public function getDependencies()
    {
        return array(
            ModuleFixtures::class,
        );
    }

    private function loadEntityBanner(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getImages('banner') as $key => $image) {

            $text = $this->fixtures->getRandomParagraph(120);
            if ($i == 2) $text = '';
            if ($i == 3) $text = $this->fixtures->getRandomParagraph(120, 'class="text-white"');
            

            $entity = new Infoblock();
            $entity->setTitle( $this->fixtures->getRandomTitle() );
            $entity->setDescription( $text );
            $entity->setLink( $this->fixtures->getRandomLink() );
            $entity->setImage($image);
            $entity->setSort($i);

            foreach ($this->modules as $key2 => $module) {
                if ($module->getName() === 'infoblock' && $module->getTemplate() === 'banner') {
                    $entity->setModule($module);
                }
            }

            $manager->persist($entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadEntityAdvantage(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getImages('advantage') as $key => $image) {
            $entity = new Infoblock();
            $entity->setTitle( $this->fixtures->getRandomTitle() );
            $entity->setDescription( $this->fixtures->getRandomText(120) );
            $entity->setLink( $this->fixtures->getRandomLink() );
            $entity->setImage($image);
            $entity->setSort($i);

            foreach ($this->modules as $key2 => $module) {
                if ($module->getName() === 'infoblock' && $module->getTemplate() === 'advantage') {
                    $entity->setModule($module);
                }
            }

            $manager->persist($entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadEntityCounter(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getImages('counter') as $key => $image) {
            $entity = new Infoblock();
            $entity->setTitle( random_int(10, 9999) );
            $entity->setDescription( $this->fixtures->getRandomTitle() );
            $entity->setLink('');
            $entity->setImage($image);
            $entity->setSort($i);

            foreach ($this->modules as $key2 => $module) {
                if ($module->getName() === 'infoblock' && $module->getTemplate() === 'counter') {
                    $entity->setModule($module);
                }
            }

            $manager->persist($entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadEntityGallery(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getImages('license') as $key => $image) {
            $entity = new Infoblock();
            $entity->setTitle( $this->fixtures->getRandomTitle() );
            $entity->setDescription('');
            $entity->setLink('');
            $entity->setImage($image);
            $entity->setSort($i);

            foreach ($this->modules as $key2 => $module) {
                if ($module->getName() === 'infoblock' && $module->getTemplate() === 'gallery') {
                    $entity->setModule($module);
                }
            }

            $manager->persist($entity);

            $i++;
        }

        $manager->flush();
    }
}
