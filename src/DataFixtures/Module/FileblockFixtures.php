<?php

namespace App\DataFixtures\Module;

use App\Entity\Fileblock;
use App\Service\DataFixtures;
use App\DataFixtures\Module\ModuleFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class FileblockFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'fileblock';
    private $fixtures;
    private $modules;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {

        $i = 1;
        while ($this->hasReference($entityReference = ModuleFixtures::ENTITY_REFERENCE . '_' . $i)) {
            $this->modules[] = $this->getReference($entityReference);
            $i++;
        }

        $this->loadEntityPrice($manager);
    }

    public function getDependencies()
    {
        return array(
            ModuleFixtures::class,
        );
    }

    private function loadEntityPrice(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getFiles() as $key => $file) {
            $entity = new Fileblock();
            $entity->setTitle( $this->fixtures->getRandomTitle() );
            $entity->setFile($file);
            $entity->setSort($i);

            foreach ($this->modules as $key2 => $module) {
                if ($module->getName() === 'fileblock' && $module->getPosition() == 1) {
                    $entity->setModule($module);
                }
            }

            $manager->persist($entity);

            $i++;
        }

        $manager->flush();
    }
}
