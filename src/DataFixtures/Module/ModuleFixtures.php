<?php

namespace App\DataFixtures\Module;

use App\Entity\Module;
use App\Service\DataFixtures;
use App\DataFixtures\PageFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ModuleFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'module';
    private $fixtures;
    private $entities;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
        $this->loadEntityPage($manager);
    }

    public function getDependencies()
    {
        return array(
            PageFixtures::class,
        );
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->getEntityData() as [$name, $title, $description, $image, $background, $template, $position, $sort, $width, $height, $width2, $height2, $enabled]) {
            $entity = new Module();
            $entity->setName($name);
            $entity->setTitle($title);
            $entity->setDescription($description);
            $entity->setImage($image);
            $entity->setBackground($background);
            $entity->setTemplate($template);
            $entity->setPosition($position);
            $entity->setSort($sort);
            $entity->setWidth($width);
            $entity->setHeight($height);
            $entity->setWidth2($width2);
            $entity->setHeight2($height2);
            $entity->setEnabled($enabled);

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadEntityPage(ObjectManager $manager): void
    {
        $pages = [];
        $i = 1;

        while ($this->hasReference($entityReference = PageFixtures::ENTITY_REFERENCE . '_' . $i)) {
            $pages[] = $this->getReference($entityReference);
            $i++;
        }

        foreach ($pages as $key => $entity) {
            foreach ($this->entities as $key2 => $entity2) {

if (

($entity->getSlug() === '' && $entity2->getName() === 'slider' && $entity2->getSort() === 0) || 
($entity->getSlug() === '' && $entity2->getName() === 'infoblock' && $entity2->getSort() === 1) || 
($entity->getSlug() === '' && $entity2->getName() === 'textblock') || 
($entity->getSlug() === '' && $entity2->getName() === 'infoblock' && $entity2->getTemplate() === 'advantage') || 
($entity->getSlug() === '' && $entity2->getName() === 'service' && $entity2->getSort() === 4) || 
($entity->getSlug() === '' && $entity2->getName() === 'product' && $entity2->getSort() === 5) || 
($entity->getSlug() === '' && $entity2->getName() === 'action' && $entity2->getSort() === 6) || 
($entity->getSlug() === '' && $entity2->getName() === 'specialist' && $entity2->getSort() === 7) || 
($entity->getSlug() === '' && $entity2->getName() === 'gallery' && $entity2->getSort() === 8) || 
($entity->getSlug() === '' && $entity2->getName() === 'faq' && $entity2->getSort() === 9) || 
($entity->getSlug() === '' && $entity2->getName() === 'infoblock' && $entity2->getSort() === 10) || 
($entity->getController() === 'catalog' && $entity2->getName() === 'category' && $entity2->getPosition() === 5) || 
// ($entity->getController() === 'blog' && $entity2->getName() === 'category' && $entity2->getPosition() === 3) || 
($entity->getController() === 'price' && $entity2->getName() === 'fileblock' && $entity2->getPosition() === 1) || 
($entity->getController() === 'contact' && $entity2->getName() === 'contact' && $entity2->getSort() === 1) 

   )
{
    $entity->addModule($entity2);

    $manager->persist($entity);
}

            }
        }

        $manager->flush();
    }

    private function getEntityData(): array
    {
        return [
// [$name, $title, $description, $image, $background, $template, $position, $sort, $width, $height, $width2, $height2, $enabled];
['slider', '', '', '', '', '', 0, 0, 0, 0, 1920, 528, true],
['infoblock', '', '', '', '', 'banner', 0, 1, 0, 0, 0, 0, true],
['textblock', $this->fixtures->getRandomTitle(), $this->fixtures->getRandomText(), $this->fixtures->getImages('textblock')[1], $this->fixtures->getImages('textblock')[0], 'default', 0, 2, 0, 0, 0, 0, true],
['infoblock', '', '', '', '', 'advantage', 0, 3, 0, 0, 360, 270, true],
['category', '', '', '', '', '', 5, 0, 0, 0, 0, 0, true],
// ['category', '', '', '', '', '', 3, 0, 0, 0, 0, 0, true],
['fileblock', '', '', '', '', '', 1, 0, 0, 0, 0, 0, true],
['service', 'Интересные предложения', $this->fixtures->getRandomParagraph(128, 'class="text-center"'), '', '', '', 0, 4, 0, 0, 267, 200, true],
['product', 'Актуальные товары', $this->fixtures->getRandomParagraph(128, 'class="text-center"'), '', '', '', 0, 5, 0, 0, 267, 200, true],
['action', 'Акции и скидки', $this->fixtures->getRandomParagraph(128, 'class="text-center"'), '', '', '', 0, 6, 0, 0, 267, 200, true],
['specialist', 'Наши специалисты', $this->fixtures->getRandomParagraph(128, 'class="text-center"'), '', '', '', 0, 7, 0, 0, 267, 200, true],
['gallery', 'Лучшие работы', $this->fixtures->getRandomParagraph(128, 'class="text-center"'), '', '', '', 0, 8, 0, 0, 267, 200, true],
['faq', 'Частые вопросы', $this->fixtures->getRandomParagraph(128, 'class="text-center"'), '', '', '', 0, 9, 0, 0, 267, 200, true],
['infoblock', '', '', '', '', 'counter', 0, 10, 0, 0, 64, 64, true],
['contact', '', '', '', '', '', 0, 1, 0, 0, 0, 0, true],

        ];
    }
}
