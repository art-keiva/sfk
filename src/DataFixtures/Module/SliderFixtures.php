<?php

namespace App\DataFixtures\Module;

use App\Entity\Slider;
use App\Service\DataFixtures;
use App\DataFixtures\Module\ModuleFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class SliderFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'slider';
    private $fixtures;
    private $modules;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {

        $i = 1;
        while ($this->hasReference($entityReference = ModuleFixtures::ENTITY_REFERENCE . '_' . $i)) {
            $this->modules[] = $this->getReference($entityReference);
            $i++;
        }

        $this->loadEntitySlider($manager);
    }

    public function getDependencies()
    {
        return array(
            ModuleFixtures::class,
        );
    }

    private function loadEntitySlider(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getImages('slider') as $key => $image) {
            $entity = new Slider();
            $entity->setTitle( $this->fixtures->getRandomTitle() );
            $entity->setDescription( $this->fixtures->getRandomText(60) );
            $entity->setLink( $this->fixtures->getRandomLink() );
            $entity->setImage($image);
            $entity->setSort($i);

            foreach ($this->modules as $key2 => $module) {
                if ($module->getName() === 'slider' && $module->getSort() === 0) {
                    $entity->setModule($module);
                }
            }

            $manager->persist($entity);

            $i++;
        }

        $manager->flush();
    }
}
