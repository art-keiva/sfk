<?php

namespace App\DataFixtures;

use App\Entity\Spcategory;
use App\Entity\Specialist;
use App\Utils\Slugger;
use App\Service\DataFixtures;
use App\DataFixtures\SpcategoryFixtures as Categories;
use App\DataFixtures\ImageFixtures as Images;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class SpecialistFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'specialist';
    private $slugger;
    private $fixtures;
    private $entities;

    public function __construct(Slugger $slugger, DataFixtures $fixtures)
    {
        $this->slugger = $slugger;
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
        // $this->loadEntityCategory($manager);
        $this->loadEntityImages($manager);
    }

    public function getDependencies()
    {
        return array(
            Categories::class,
            Images::class,
        );
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getFullNames() as $i => $title) {
            $entity = new Specialist();
            $entity->setTitle( $title );
            $entity->setSummary( $this->fixtures->getJobs()[$i] );
            $entity->setDescription( $this->fixtures->getRandomParagraphs(random_int(4, 10)) );
            $entity->setMetaTitle( $title );
            $entity->setMetaDescription('');
            $entity->setMetaKeyword('');
            $entity->setImage( $this->fixtures->getRandomImage('specialist') );
            $entity->setSlug( $title );
            $entity->setSort( $i );
            $entity->setEnabled(true);
            $entity->setSpecial($i % 2);

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadEntityCategory(ObjectManager $manager): void
    {
        $categories = [];
        $i = 1;

        while ($this->hasReference($entityReference = Categories::ENTITY_REFERENCE . '_' . $i)) {
            $category = $this->getReference($entityReference);
            if (null !== $category->getParent()) {
                $categories[] = $this->getReference($entityReference);
            }
            $i++;
        }

        if ($categories) {
            foreach ($this->entities as $key => $entity) {
                $randomEntityKey = array_rand($categories);
                $entity->addSpcategory($categories[$randomEntityKey]);
                $entity->addSpcategory($categories[0]); // and add all product into first category for tests

                $manager->persist($entity);
            }
        }

        $manager->flush();
    }

    private function loadEntityImages(ObjectManager $manager): void
    {
        $images = [];
        $i = 1;

        while ($this->hasReference($entityReference = Images::ENTITY_REFERENCE . '_' . $i)) {
            $images[] = $this->getReference($entityReference);
            $i++;
        }

        if ($images) {
            foreach ($this->entities as $key => $entity) {
                // $randomEntityKey = array_rand($images);
                foreach ($images as $key2 => $entity2) {
                    // supposed 11 images in db and we split them between service, product, specialist entitites
                    if ($key2 >= 8) {
                        $entity->addImage($entity2); // and add all images into first entity for tests
                    }
                }

                $manager->persist($entity);

                break;
            }
        }

        $manager->flush();
    }
}
