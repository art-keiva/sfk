<?php

namespace App\DataFixtures;

use App\Entity\Image;
use App\Service\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ImageFixtures extends Fixture
{
    public const ENTITY_REFERENCE = 'image';
    private $fixtures;
    private $entities;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getImages() as $i => $image) {
            $entity = new Image();
            $entity->setTitle( $this->fixtures->getRandomTitle() );
            $entity->setImage( $image );
            $entity->setSort( $i );

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }
}
