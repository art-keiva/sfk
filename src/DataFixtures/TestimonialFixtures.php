<?php

namespace App\DataFixtures;

use App\Entity\Testimonial;
use App\Service\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TestimonialFixtures extends Fixture
{
    public const ENTITY_REFERENCE = 'testimonial';
    private $fixtures;
    private $entities;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getFullNames() as $i => $title) {
            $entity = new Testimonial();
            $entity->setTitle( $title );
            $entity->setDescription( $this->fixtures->getRandomParagraph() );
            $entity->setImage( $this->fixtures->getRandomImage('testimonial') );
            $entity->setSort( $i );
            $entity->setEnabled(true);
            $entity->setSpecial($i % 2);

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }
}
