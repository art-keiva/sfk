<?php

namespace App\DataFixtures;

use App\Entity\Faq;
use App\Service\DataFixtures;
use App\DataFixtures\FtagFixtures as Categories;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class FaqFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'faq';
    private $fixtures;
    private $entities;

    public function __construct(DataFixtures $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadEntities($manager);
        $this->loadEntityCategory($manager);
    }

    public function getDependencies()
    {
        return array(
            Categories::class,
        );
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 1;
        foreach ($this->fixtures->getPhrases() as $i => $title) {
            $entity = new Faq();
            $entity->setQuestion( $title );
            $entity->setAnswer( $this->fixtures->getRandomParagraph() );
            $entity->setSort( $i );
            $entity->setEnabled(true);
            $entity->setSpecial($i % 2);

            $manager->persist($entity);

            $this->entities[] = $entity;

            $this->addReference(self::ENTITY_REFERENCE . '_' . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function loadEntityCategory(ObjectManager $manager): void
    {
        $categories = [];
        $i = 1;

        while ($this->hasReference($entityReference = Categories::ENTITY_REFERENCE . '_' . $i)) {
            $categories[] = $this->getReference($entityReference);
            $i++;
        }

        if ($categories) {
            foreach ($this->entities as $key => $entity) {
                $randomEntityKey = array_rand($categories);
                $entity->setFtag($categories[$randomEntityKey]);

                $manager->persist($entity);
            }
        }

        $manager->flush();
    }
}
