<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Type;

use App\Controller\Admin\PageController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use App\Form\DataTransformer\TagArrayToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Defines the custom form field type used to manipulate tags values across
 * Bootstrap-tagsinput javascript plugin.
 *
 * See https://symfony.com/doc/current/cookbook/form/create_custom_field_type.html
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class ControllerType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // $controllerList = [];

        // foreach (PageController::getControllers() as $key => $value) {
        //     $controllerList[$value] = $key;
        // }
        $builder
            ->add('controller', ChoiceType::class, [
                //  'choices'  => [
                //     'Maybe' => null,
                //     'Yes' => true,
                //     'No' => false,
                // ]
                // 'choices' => $controllerList,
            'empty_data' => '',
                'attr'   => [
                    'choices'  => [
                    'Maybe' => null,
                    'Yes' => true,
                    'No' => false,
                ]
                    // 'placeholder' => '',
                    // 'required'   => false,
                    // 'empty_data'  => '',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    // public function buildView(FormView $view, FormInterface $form, array $options): void
    // {
    //     $view->vars['tags'] = $this->tags->findAll();
    // }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
