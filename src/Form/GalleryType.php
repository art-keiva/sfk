<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FM\ElfinderBundle\Form\Type\ElFinderType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Contracts\Translation\TranslatorInterface;

class GalleryType extends AbstractType {

    private $translator;

    public function __construct(TranslatorInterface $translator) {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('image', ElFinderType::class, [
                // 'label' => $this->translator->trans('Image', [], 'admin'),
                'label' => false,
                'enable' => true,
                'instance' => 'form',
                'attr' => [
                    'entity' => 'gallery',
                ],
            ])
            ->add('title', TextareaType::class, [
                'label' => $this->translator->trans('label.title_photo', [], 'admin'),
                // 'css_class' => 'large',
                'attr' => [
                    // 'required' => false,
                    'class' => 'large'
                ],
            ])
            ->add('sort', null, [
                'empty_data' => '0',
                // 'class' => 'col-sm-12 555'
                'attr' => [
                    'required' => false,
                    'class' => 'col-sm-12 555'
                ],
            ])
            ->add('enabled', CheckboxType::class, [
                // 'data' => true,
                'attr'   => [
                    'required'   => false,
                    // 'checked'   => 'checked',
                    // 'class' => 'col-sm-6',
                ],
            ])
            ->add('special', CheckboxType::class, [
                // 'data' => true,
                'attr'   => [
                    'required'   => false,
                    // 'checked'   => 'checked',
                    // 'class' => 'col-sm-6',
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Gallery'
        ));
    }

}
