<?php

namespace App\Form\DataTransformer;

use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ControllerTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (page) to a string (number).
     *
     * @param  Page|null $page
     * @return string
     */
    public function transform($page)
    {
        if (null === $page) {
            return '';
        }

        return $page->getId();
    }

    /**
     * Transforms a string (number) to an object (page).
     *
     * @param  string $controller
     * @return Page|null
     * @throws TransformationFailedException if object (page) is not found.
     */
    public function reverseTransform($controller)
    {
        // no page number? It's optional, so that's ok
        if (!$controller) {
            return;
        }

        $page = $this->entityManager
            ->getRepository(Page::class)
            // query for the page with this id
            ->find($controller)
        ;

        if (null === $page) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An page with number "%s" does not exist!',
                $controller
            ));
        }

        return $page;
    }
}
