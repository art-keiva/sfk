<?php

namespace App\Form;

use App\Entity\Faq;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class FaqType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', TextareaType::class, [
                'attr'   => [
                    'required'   => true,
                    // 'class' => 'col-sm-12 555'
                ],
            ])
            ->add('answer', CKEditorType::class, [
                'empty_data'   => '',
                'attr'   => [
                    'required'   => false,
                    // 'class' => 'col-sm-6'
                ],
            ])
            ->add('sort', null, [
                'empty_data'   => '0',
                'attr'   => [
                    'required'   => false,
                    // 'class' => 'col-sm-6'
                ],
            ])
            ->add('enabled', CheckboxType::class, [
                // 'data' => true,
                'attr'   => [
                    'required'   => false,
                    // 'checked'   => 'checked',
                    // 'class' => 'col-sm-6',
                ],
            ])
            ->add('special', CheckboxType::class, [
                // 'data' => true,
                'attr'   => [
                    'required'   => false,
                    // 'checked'   => 'checked',
                    // 'class' => 'col-sm-6',
                ],
            ])
        ;

        $builder->get('enabled')->setData( true );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Faq::class,
            'attr'   => [
                // 'class' => 'col-sm-12 555'
            ],
        ));
    }
}