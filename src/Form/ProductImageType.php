<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FM\ElfinderBundle\Form\Type\ElFinderType;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProductImageType extends AbstractType {

    private $translator;

    public function __construct(TranslatorInterface $translator) {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('image', ElFinderType::class, [
                    // 'label' => $this->translator->trans('Image', [], 'admin'),
                    'label' => false,
                    'enable' => true,
                    'instance' => 'form',
                    // 'attr' => [
                    //     'entity' => 'product',
                    // ],
                ])
                ->add('title')
                ->add('sort', null, [
                    'empty_data' => '0',
                    'attr' => [
                        'required' => false,
                    ],
                ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Image',
        ));
    }

}
