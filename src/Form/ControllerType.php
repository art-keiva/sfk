<?php

namespace App\Form;

use App\Entity\Page;
use App\Controller\Admin\PageController;
use App\Form\DataTransformer\ControllerTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ControllerType extends AbstractType
{
    private $transformer;

    public function __construct(ControllerTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $controllerList = [];

        foreach (PageController::getControllers() as $key => $value) {
            $controllerList[$value] = $key;
        }

        $builder
            ->add('controller', ChoiceType::class, [
                 'choices'  => [
                    'Maybe' => null,
                    'Yes' => true,
                    'No' => false,
                ]
                // 'choices' => $controllerList,
                // 'attr'   => [
                    // 'choices' => $controllerList,
                    // 'preferred_choices' => ['default'],
                    // 'placeholder' => '',
                    // 'required'   => false,
                    // 'empty_data'  => '',
                // ],
            ])
        ;

        $builder->get('controller')
            ->addModelTransformer($this->transformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            // 'inherit_data' => true,
            'data_class' => Page::class,
        ));
    }
}