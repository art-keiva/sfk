<?php

namespace App\Form;

use App\Entity\Info;
// use App\Form\Type\InfoRowsType;
use App\Repository\InfoRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class InfoType extends AbstractType
{
    private $infos;

    public function __construct(InfoRepository $infos)
    {
        $this->infos = $infos;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // dump($options);
        
        $results = $this->infos->findAll();
        $infoKeys = [];

        foreach ($results as $key => $value) {
            $infoKeys[] = $value->getKey();
        }
        // $builder
        //         ->add('info', CollectionType::class, [
        //             // 'label' => $value->getName(),
        //             // 'data' => $value->getValue(),
        //             'entry_type' => TextType::class,
        //             'required' => false,
        //             'entry_options' => [
        //                 'attr' => ['class' => 'email-box'],
        //             ],
        //             // 'property_path' => $value->getKey() . '['.$key.']',
        //         ]);

            $imagesType = ['logo'];
            $textareaType = ['address'];
            $textareaEditorType = ['comment'];
        foreach ($results as $key => $value) {
            // $view->vars[$value->getKey()][$value->getName()] = $value->getValue();
            

            // Text
            $fieldType = TextType::class;

            // Images
            if (in_array($value->getName(), $imagesType)) {
                $fieldType = VichImageType::class;
            }

            // Textarea
            if (in_array($value->getName(), $textareaType)) {
                $fieldType = TextAreaType::class;
            }

            // Textarea with editor
            if (in_array($value->getName(), $textareaEditorType)) {
                $fieldType = CKEditorType::class;
            }

            $fieldName = $value->getKey() . ':' . $value->getName() ;
        
            $builder
                // ->add($value->getKey(), TextType::class, [
                ->add($fieldName, $fieldType, [
                    'label' => 'label.' . $value->getKey() . '.' . $value->getName(),
                    'data' => $value->getValue(),
                    'required' => false,
                        'attr' => [
                            // 'class' => 'text-box',
                            'data-parent' => $value->getKey(),
                        ],
                    // 'property_path' => $value->getKey() . '['.$key.']',
                ]);
        }

        // $results = array_unique($infoKeys);

        // foreach ($results as $key => $value) {
        //     // $view->vars[$value->getKey()][$value->getName()] = $value->getValue();
        
        //     $builder
        //         ->add($value, CollectionType::class, [
        //             'allow_add' => TextType::class,
        //             'entry_type' => TextType::class,
        //             'entry_options' => [
        //                 'attr' => ['class' => 'text-box'],
        //             ],
        //         ]);
        // }
        // dump($builder);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }
}