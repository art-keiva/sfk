<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use FM\ElfinderBundle\Form\Type\ElFinderType;
use Symfony\Contracts\Translation\TranslatorInterface;

class ElFinderImageType extends AbstractType
{

    private $translator;

    public function __construct(TranslatorInterface $translator) {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', ElFinderType::class, [
                'label' => false,
                'enable' => true,
                'instance' => 'form',
                // 'attr' => [
                //     'entity' => 'service',
                // ],
            ])
            // ->add('choose', ButtonType::class, [
            //     'label' => $this->translator->trans('action.choose_file', [], 'admin'),
            // ])
            // ->add('sort', ElFinderType::class, [
            //     'instance' => '0',
            //     'attr' => [
            //         'required' => false,
            //     ],
            // ])
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        // dump($options);
        // $parentData = $form->getParent()->getData();

        // $view->vars['image_uri'] = null;

        // if (null !== $parentData) {
        //     $view->vars['image_uri'] = $parentData->getImage();
        // }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            // 'data_class' => null,
            'inherit_data' => true,
        ));
    }
}