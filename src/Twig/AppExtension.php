<?php

namespace App\Twig;

use App\Utils\Info;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Liip\ImagineBundle\Exception\Imagine\Filter\NonExistingFilterException;

class AppExtension extends AbstractExtension
{
    private $liipImagine;
    private $publicDir;
    private $defaultPath;
    private $filterManager;
    private $info;

    public function __construct($liipImagine, $publicDir, $defaultPath, FilterManager $filterManager, Info $info)
    {
        $this->liipImagine = $liipImagine;
        $this->publicDir = $publicDir;
        $this->defaultPath = $defaultPath;
        $this->filterManager = $filterManager;
        // $this->filesystem = $filesystem;
        $this->info = $info;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('default_image', [$this, 'getDefaultImage']),
            // new TwigFilter('image_filter', [$this, 'getImageFilter']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('image_filter', [$this, 'getImageFilter']),
        ];
    }

    public function getDefaultImage(string $path): string
    {
        if (is_file($this->publicDir . '/' . $path)) {
            return $path;
        }

        return $this->defaultPath . '/no_image.png';
    }

    public function getImageFilter(string $imageName, ?string $liipFilter = '', ?array $runtimeConfig = [], ?string $imagePath = ''): string
    {

        /**
         * Output default image if input image is not exists.
         */
        // if (null === $imagePath || empty($imagePath)) {
        //     $imagePath = $this->defaultPath;
        // }

        $imageWithPath = $imagePath . '/' . $imageName;

        /**
         * Output default image if input image is not exists.
         */
        if (empty($imageName) || !is_file($this->publicDir . $imagePath . $imageName)) {
            $imageWithPath = $this->getDefaultImage($imageWithPath);
        }


        /**
         * throw exception if imagine filter is not exists
         */
        $defaultFilter = 'default';
        $filters = $this->filterManager->getFilterConfiguration()->all();

        if (null === $liipFilter || empty($liipFilter)) {
            $liipFilter = $defaultFilter;
        }

        if (false === array_key_exists($liipFilter, $filters)) {
            throw new NonExistingFilterException(sprintf('Please add the filter with name: %s, or add at least one of the filter in liip_imagine configuration yaml file and use it with second parameter', $liipFilter));
        }

        /**
         * all scripts below for output cache file
         */
        $mode = 'inset';

        if (isset($this->info->size['mode_' . $liipFilter]) && $this->info->size['mode_' . $liipFilter] === '1') {
            $mode = 'outbound';
        }

        /**
         * [$sizes set sizes if they exists in db or indicates directrly]
         * @var array
         */
        $sizes = [];

        if (isset($this->info->size) && isset($this->info->size['width_' . $liipFilter]) && isset($this->info->size['height_' . $liipFilter])) {
            $sizes = [
                $this->info->size['width_' . $liipFilter],
                $this->info->size['height_' . $liipFilter],
            ];
        }

        if (null !== $runtimeConfig && !empty($runtimeConfig)) {
            if ((is_int($runtimeConfig[0]) && $runtimeConfig[0] > 0) && (is_int($runtimeConfig[1]) && $runtimeConfig[1] > 0)) {
                $sizes = [
                    $runtimeConfig[0],
                    $runtimeConfig[1],
                ];
            }
        }

        $runtimeConfig = [];

        if (!empty($sizes)) {
            $runtimeConfig = [
                'thumbnail' => [
                    'size' => $sizes,
                    'mode' => $mode,
                    'allow_upscale' => true,
                ],
                'background' => [
                    'size' => $sizes,
                    'position' => 'center',
                    'color' => '#ffffff',
                ],
            ];
        }

        /**
         * Output cache file with path
         */
        return$this->liipImagine->getUrlOfFilteredImageWithRuntimeFilters(
            $imageWithPath,
            $liipFilter,
            $runtimeConfig
        );
    }
}
