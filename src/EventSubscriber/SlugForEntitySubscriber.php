<?php

namespace App\EventSubscriber;

use App\Entity\Slug;
use App\Utils\Slugger;
use Doctrine\ORM\Events;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class SlugForEntitySubscriber implements EventSubscriber
{
    public $slugger;
    public $entityId;

    public function __construct(EntityManagerInterface $entityManager, Slugger $slugger)
    {
        $this->slugger = $slugger;
    }

    public function getSubscribedEvents()
    {
        /**
         * more events:
         * https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/events.html#lifecycle-events
         */
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::preRemove,
            Events::postRemove,
            Events::postLoad,
        ];
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();
        $slugRepo = $args->getEntityManager()->getRepository(Slug::class);
        $entityClass = get_class($entity);

        if ($this->slugger->hasMethodsAndConsts($entityClass)) {
            $findedSlug = $slugRepo->findOneByEntity($entityClass::ROUTE, $entity->getId(), $entityClass::IS_LIST);
            $entity->setSlug($findedSlug->getSlug());
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();
        $em = $args->getEntityManager();
        $slugRepo = $em->getRepository(Slug::class);
        $entityClass = get_class($entity);

        if ($this->slugger->hasMethodsAndConsts($entityClass)) {

            // add slug
            if (null === $entity->getSlug() || '' === $entity->getSlug()) {
                $findedSlug = $slugRepo->findOneBySlug('');
                if (null !== $findedSlug) {
                    $entity->setSlug($entity->getTitle());
                }
            }

            $slugifiedTitle = $this->slugger->slugify($entity->getSlug());

            $entity->setSlug($slugifiedTitle);

            $newSlug = new Slug();
            $newSlug->setRoute($entityClass::ROUTE);
            $newSlug->setEntityId($entity->getId());
            $newSlug->setIsList($entityClass::IS_LIST);
            $newSlug->setSlug($entity->getSlug());

            // duplicate title for meta-title 
            if (null === $entity->getMetaTitle() || '' === $entity->getMetaTitle()) {
                $entity->setMetaTitle($entity->getTitle());
            }

            $em->persist($newSlug);
            $em->flush();
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();
        $em = $args->getEntityManager();
        $slugRepo = $em->getRepository(Slug::class);
        $entityClass = get_class($entity);

        if ($this->slugger->hasMethodsAndConsts($entityClass)) {

            if (null === $entity->getSlug() || '' === $entity->getSlug()) {
                $entity->setSlug($entity->getTitle());
            }

            $findedSlug = $slugRepo->findOneByEntity($entityClass::ROUTE, $entity->getId(), $entityClass::IS_LIST);

            // leave empty slug if it is the homepage
            if (empty($findedSlug->getSlug())) {
                return;
            }

            $slugifiedTitle = $this->slugger->slugify($entity->getSlug(), $findedSlug->getId());

            $findedSlug->setSlug($slugifiedTitle);

            $em->persist($findedSlug);
            $em->flush();
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof Slug) {
            $this->entityId = $entity->getId();
        } else {
            $this->entityId = null;
        }
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        if (null === $this->entityId) {
            return;
        }

        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();
        $em = $args->getEntityManager();
        $slugRepo = $em->getRepository(Slug::class);
        $entityClass = get_class($entity);

        if ($this->slugger->hasMethodsAndConsts($entityClass)) {
            $findedSlug = $slugRepo->findOneByEntity($entityClass::ROUTE, $this->entityId, $entityClass::IS_LIST);

            if (!is_null($findedSlug)) {
                $em->remove($findedSlug);
                $em->flush();
            }
        }
    }
}
