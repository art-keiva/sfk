<?php

namespace App\EventSubscriber;

// use App\Entity\Slug;
// use App\Repository\SlugRepository;
// use App\Utils\Slugger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
// use Symfony\Component\EventDispatcher\GenericEvent;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    // private $slugRepository;
    // private $slugger;

    // public function __construct(SlugRepository $slugRepository, Slugger $slugger)
    // {
    //     $this->slugRepository = $slugRepository;
    //     $this->slugger = $slugger;
    // }

    public static function getSubscribedEvents()
    {
        return array(
            // 'easy_admin.post_persist' => array('newSlug'),
            // 'easy_admin.post_update' => array('updateSlug'),
            // 'easy_admin.post_delete' => array('deleteSlug'),
            // 'easy_admin.pre_edit' => array('getSlug'),
            // 'easy_admin.pre_persist' => array('setBlogPostSlug'),
        );
    }

    // public function newSlug(GenericEvent $event)
    // {
    //     $entity = $event->getSubject();
    //     $entityClass = get_class($entity);

    //     if (method_exists($entityClass, 'getSlug')) {
    //         if (is_null($entity->getSlug())) {
    //             $entity->setSlug($entity->getTitle());
    //         }
    //         $slugifiedTitle = $this->slugger->slugify($entity->getSlug());
    //         $entity->setSlug($slugifiedTitle);

    //         if (!defined("$entityClass::ROUTE")) {
    //             throw new \Exception('The class "%s" not have const "ROUTE"!', $entityClass);
    //         }
    //         if (!defined("$entityClass::IS_LIST")) {
    //             throw new \Exception('The class "%s" not have const "IS_LIST"!', $entityClass);
    //         }

    //         $em = $event->getArgument('em');
    //         $newSlug = new Slug();
    //         $newSlug->setRoute($entityClass::ROUTE);
    //         $newSlug->setEntityId($entity->getId());
    //         $newSlug->setIsList($entityClass::IS_LIST);
    //         $newSlug->setSlug($entity->getSlug());

    //         $em->persist($newSlug);
    //         $em->flush();
    //     }

    //     $event['entity'] = $entity;
    // }

/*
    public function updateSlug(GenericEvent $event)
    {
        $entity = $event->getSubject();
        $entityClass = get_class($entity);

        if (method_exists($entityClass, 'getSlug')) {
            if (is_null($entity->getSlug())) {
                $entity->setSlug($entity->getTitle());
            }
            $slugifiedTitle = $this->slugger->slugify($entity->getSlug());
            $entity->setSlug($slugifiedTitle);

            if (!defined("$entityClass::ROUTE")) {
                throw new \Exception('The class "%s" not have const "ROUTE"!', $entityClass);
            }
            if (!defined("$entityClass::IS_LIST")) {
                throw new \Exception('The class "%s" not have const "IS_LIST"!', $entityClass);
            }

            $em = $event->getArgument('em');
            $slug = $this->slugRepository->findOneByEntity($entityClass::ROUTE, $entity->getId(), $entityClass::IS_LIST);
            if (!is_null($slug)) {
                $slug->setSlug($entity->getSlug());
                $em->persist($slug);
                $em->flush();
            }
        }

        $event['entity'] = $entity;
    }
*/

    // public function deleteSlug(GenericEvent $event)
    // {
    //     $entity = $event->getSubject();
    //     $request = $event->getArgument('request');
    //     $entityClass = $entity['class'];

    //     if (method_exists($entityClass, 'getSlug')) {

    //         if (!defined("$entityClass::ROUTE")) {
    //             throw new \Exception('The class "%s" not have const "ROUTE"!', $entityClass);
    //         }
    //         if (!defined("$entityClass::IS_LIST")) {
    //             throw new \Exception('The class "%s" not have const "IS_LIST"!', $entityClass);
    //         }

    //         $em = $event->getArgument('em');
    //         $slug = $this->slugRepository->findOneByEntity($entityClass::ROUTE, $request->query->get('id'), $entityClass::IS_LIST);

    //         if (!is_null($slug)) {
    //             $em->remove($slug);
    //             $em->flush();
    //         }
    //     }

    //     $event['entity'] = $entity;
    // }
}
