<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use Behat\Transliterator\Transliterator;

class FileUploader
{
    private $imageDir;
    private $fileDir;

    public function __construct($imageDir, $fileDir)
    {
        $this->imageDir = $imageDir;
        $this->fileDir = $fileDir;
    }

    public function upload(UploadedFile $file, bool $isImage = true)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = Transliterator::transliterate($originalFilename);
        // $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $fileExtension = $file->guessExtension();
        if ($fileExtension === 'jpeg') {
            $fileExtension = 'jpg';
        }
        $fileName = $safeFilename.'-'.uniqid().'.'.$fileExtension;

        try {
            $targetDirectory = $isImage ? $this->getImageDir() : $this->getFileDir() ;
            $file->move($targetDirectory, $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
            throw new \Exception('something happens during file upload.');
        }

        return $fileName;
    }

    public function remove(string $filename, bool $isImage = true)
    {

        $finder = new Finder();
        $filesystem = new Filesystem();

        $targetDirectory = $isImage ? $this->getImageDir() : $this->getFileDir() ;

        $finder->in($targetDirectory);

        $filename = rtrim($filename, '/');

        if (!empty($filename) && $finder->files()->name($targetDirectory . '/' . $filename)) {
            try {
                $filesystem->remove($targetDirectory . '/' . $filename);
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
                throw new \Exception('something happens during file remove.');
            }
        }

        return true;
    }

    public function getImageDir()
    {
        return $this->imageDir;
    }

    public function getFileDir()
    {
        return $this->fileDir;
    }
}
