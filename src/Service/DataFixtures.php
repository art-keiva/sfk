<?php

namespace App\Service;

use Symfony\Component\HttpKernel\KernelInterface;

class DataFixtures
{
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function getRandomTitle(): string
    {
        $phrases = $this->getPhrases();
        $randomTitle = $phrases[array_rand($phrases)];

        return $randomTitle;
    }

    public function getRandomText(int $maxLength = 255): string
    {
        $phrases = $this->getPhrases();
        shuffle($phrases);

        while (mb_strlen($randomText = implode('. ', $phrases).'.') > $maxLength) {
            array_pop($phrases);
        }

        return $randomText;
    }

    public function getRandomTag(): string
    {
        $tagNames = $this->getTags();
        $randomTag = $tagNames[array_rand($tagNames)];

        return $randomTag;
    }

    public function getRandomTags(): array
    {
        $tagNames = $this->getTags();
        shuffle($tagNames);
        $randomTags = \array_slice($tagNames, 0, random_int(2, 4));

        return $randomTags;
    }

    public function getRandomLink(): string
    {
        $links = $this->getLinks();
        $randomLink = $links[array_rand($links)];

        return $randomLink;
    }

    public function getRandomImage(string $entity = ''): string
    {
        $imageNames = $this->getImages($entity);
        $randomImage = $imageNames[array_rand($imageNames)];

        return $randomImage;
    }

    public function getRandomImages(string $entity = ''): array
    {
        $imageNames = $this->getImages($entity);
        shuffle($imageNames);
        $randomImages = \array_slice($imageNames, 0, random_int(2, 4));

        return $randomImages;
    }

    public function getRandomParagraph(int $maxLength = 255, string $attr = ''): string
    {
        $randomText = $this->getRandomText($maxLength);
        $format = '<p ' . $attr . '>%s</p>' . PHP_EOL;
        $randomParagraph = sprintf($format, $randomText);

        return $randomParagraph;
    }

    public function getRandomParagraphs(int $maxRepeats = 3): string
    {
        $randomParagraphs = '';

        for ($i = 1; $i <= $maxRepeats; $i++) {
            $randomParagraphs .= $this->getRandomParagraph(random_int(128, 512));
        }

        return $randomParagraphs;
    }

    public function getLinks(): array
    {
        return [
            'https://www.google.com',
            'https://wikipedia.org',
            'https://www.amazon.com',
            'https://www.youtube.com',
        ];
    }

    public function getTags(): array
    {
        return [
            'опыт',
            'создание',
            'Разнообразный',
            'реализация',
            'обуславливает',
            'значение',
            'систем',
            'практика',
            'предложений',
        ];
    }

    public function getImages(string $entity = ''): array
    {
        switch ($entity) {
            case 'service':
                $images = [
                    'services1.jpg',
                    'services2.jpg',
                    'services3.jpg',
                    'services4.jpg',
                    'services5.jpg',
                    'services6.jpg',
                ];
                break;

            case 'specialist':
                $images = [
                    'worker1.jpg',
                    'worker2.jpg',
                    'worker3.jpg',
                    'worker4.jpg',
                ];
                break;

            case 'blog':
                $images = [
                    'blog2.jpg',
                    'blog3.jpg',
                    'blog4.jpg',
                    'blog5.jpg',
                    'blog2.jpg',
                ];
                break;

            case 'gallery':
                $images = [
                    'services1.jpg',
                    'services2.jpg',
                    'services3.jpg',
                    'services4.jpg',
                    'services5.jpg',
                    'services6.jpg',
                ];
                break;

            case 'testimonial':
                $images = [
                    'certificate-1.jpg',
                    'certificate-2.jpg',
                    'certificate-3.jpg',
                    'certificate-4.jpg',
                    'certificate-5.jpg',
                    'certificate-6.jpg',
                ];
                break;

            case 'license':
                $images = [
                    'license.jpg',
                    'license.jpg',
                    'license.jpg',
                ];
                break;

            case 'certificate':
                $images = [
                    'certificate-1.jpg',
                    'certificate-2.jpg',
                    'certificate-3.jpg',
                    'certificate-4.jpg',
                    'certificate-5.jpg',
                    'certificate-6.jpg',
                ];
                break;

            case 'textblock':
                $images = [
                    'doted-map3.jpg',
                    'services2.jpg',
                ];
                break;

            case 'slider':
                $images = [
                    'slider-truck-test2.jpg',
                    'slider-bus.jpg',
                    'slider-van.jpg',
                ];
                break;

            case 'banner':
                $images = [
                    '1231.jpg',
                    'banner-3.gif',
                    '1232.jpg',
                ];
                break;

            case 'counter':
                $images = [
                    'comments.png',
                    'correct-symbol.png',
                    'suitcase-with-white-details.png',
                    'thumbs-up.png',
                ];
                break;

            case 'advantage':
                $images = [
                    'services3.jpg',
                    'services5.jpg',
                    'services6.jpg',
                ];
                break;

            case 'contact':
                $images = [
                    'street_1.jpg',
                    'street_2.jpg',
                    'street_3.jpg',
                ];
                break;
            
            default:
                $images = [
                    'hp_3.jpg',
                    'htc_touch_hd_1.jpg',
                    'iphone.jpg',
                    'macbook.jpg',
                    'nikon_d300_5.jpg',
                    'palm_treo_pro_1.jpg',
                    'samsung_tab_7.jpg',
                    'samsungtab.jpg',
                    'sony_vaio_4.jpg',
                    'sony_vaio_5.jpg',
                    'thunderboltdisplay.jpg',
                ];
                break;
        }

        if ($this->kernel->getContainer()->hasParameter('app.path.media_images')) {
            $media_images = $this->kernel->getContainer()->getParameter('app.path.media_images');
        }

        $path = $media_images ?? '';

        if (!empty($path)) {
            foreach ($images as $key => $value) {
                $images[$key] = $media_images . '/' . $value;
            }
        }

        return $images;
    }

    public function getFiles(): array
    {
        return [
            'test.docx',
            'test.pdf',
            'test.xlsx',
        ];
    }

    public function getPhrases(): array
    {
        return [
            'Разнообразный и богатый опыт',
            'Консультация с широким активом',
            'Реализация намеченных',
            'Плановых заданий',
            'Равным образом',
            'Дальнейшее развитие',
            'Различных форм деятельности',
            'Представляет собой интересный',
            'Эксперимент проверки систем',
            'Массового участия',
            'Идейные соображения',
            'Высшего порядка',
            'Дальнейшее развитие',
            'Различных форм деятельности',
            'Играет важную роль',
            'Системы обучения кадров',
            'Соответствует насущным потребностям',
            'Равным образом дальнейшее',
            'Развитие различных форм',
            'Деятельности требуют определения',
            'Уточнения направлений прогрессивного',
            'Таким образом рамки',
            'Место обучения кадров',
            'Представляет собой интересный',
            'Эксперимент проверки',
            'Существенных финансовых',
            'Административных условий',
            'Не следует, однако забывать',
            'Новая модель организационной деятельности',
            'Значимость этих проблем',
        ];
    }

    public function getShortPhrases(): array
    {
        return [
            'Реализация намеченных',
            'Плановых заданий',
            'Равным образом',
            'Эксперимент проверки',
            'Существенных финансовых',
            'Таким образом рамки',
            'Место обучения кадров',
            'Массового участия',
            'Идейные соображения',
            'Высшего порядка',
            'Дальнейшее развитие',
            'Представляет собой',
            'Административных',
            'Уточнения направлений',
            'Развитие различных',
            'Эксперимент проверки',
            'Организационной',
            'Требуют определения',
        ];
    }

    public function getLongPhrases(): array
    {
        return [
            'Давно выяснено, что при оценке дизайна',
            'Композиции читаемый текст мешает сосредоточиться',
            'Используют потому, что тот обеспечивает',
            'Более или менее стандартное заполнение шаблона',
            'Также реальное распределение букв и пробелов',
            'Которое не получается при простой дубликации',
            'Многие программы электронной вёрстки и редакторы',
            'Сразу показывает, как много веб-страниц',
            'Всё ещё дожидаются своего настоящего рождения',
            'Некоторые версии появились по ошибке',
            'Часто используемый в печати и вэб-дизайне',
            'В то время некий безымянный печатник создал',
            'Большую коллекцию размеров и форм шрифтов',
            'Идейные соображения высшего порядка',
            'Также дальнейшее развитие различных форм',
            'Деятельности играет важную роль',
            'В формировании системы обучения кадров',
            'Соответствует насущным потребностям',
            'Не следует, однако забывать',
            'Что новая модель организационной деятельности обеспечивает',
            'Участие в формировании соответствующий',
            'Значимость этих проблем настолько очевидна',
            'Повседневная практика показывает',
            'Начало повседневной работы по формированию позиции',
            'Позволяет оценить значение новых предложений',
            'Задача организации, в особенности',
            'Равным образом постоянное информационно-пропагандистское',
            'Обеспечение нашей деятельности требуют',
            'Определения и уточнения соответствующий',
            'Представляет собой интересный эксперимент',
        ];
    }

    public function getPostContent(): string
    {
        return <<<'MARKDOWN'
Значимость этих проблем настолько очевидна, что постоянное информационно-пропагандистское
обеспечение **нашей деятельности** позволяет выполнять важные задания по разработке системы
обучения кадров, соответствует насущным потребностям. Повседневная практика показывает,
что дальнейшее развитие различных форм деятельности позволяет оценить значение систем
массового участия.

  * Разнообразный и богатый опыт
  * Реализация намеченных *плановых заданий*
  * Начало повседневной работы

Повседневная практика показывает, что начало повседневной работы по формированию
позиции позволяет оценить значение новых предложений. Задача организации,
в особенности же начало повседневной работы по формированию позиции обеспечивает
широкому кругу участие в формировании направлений прогрессивного развития.

Идейные соображения высшего порядка, а также дальнейшее развитие различных форм
деятельности играет важную роль в формировании *системы обучения кадров*,
соответствует насущным потребностям. Равным образом дальнейшее развитие
различных форм деятельности требуют определения и уточнения направлений
прогрессивного развития.

Товарищи! новая модель организационной деятельности требуют от нас анализа
существенных финансовых и административных условий. **Таким образом** начало
повседневной работы по формированию позиции позволяет оценить
значение систем массового участия.

С другой стороны реализация намеченных плановых заданий обеспечивает
широкому кругу участие в формировании направлений прогрессивного
развития. Повседневная практика показывает,что реализация
намеченных плановых заданий представляет собой интересный эксперимент
проверки направлений прогрессивного развития.
MARKDOWN;
    }

    public function getFullNames(): array
    {
        return [
            'Самойлов Болеслав Романович',
            'Яровой Еремей Алексеевич',
            'Панфилов Устин Андреевич',
            'Якушев Адам Вадимович',
            'Буров Устин Валерьевич',
            'Борисенко Бронислав Юхимович',
            'Родионов Ростислав Богданович',
            'Дроздов Давид Сергеевич',
            'Дорофеев Виль Сергеевич',
            'Борисов Владлен Львович',
            'Савенко Корнелий Леонидович',
            'Кравчук Евсей Викторович',
            'Николаев Пётр Иванович',
        ];
    }

    public function getJobs(): array
    {
        return [
            'Бизнес-аналитик',
            'Химик-технолог',
            'Фармацевт',
            'Менеджер по продажам',
            'Офис-менеджер',
            'Главный конструктор',
            'Станочник широкого профиля',
            'Кровельщик',
            'Аппаратчик-оператор',
            'Маркетолог',
            'Страховой агент',
            'Преподаватель',
            'Финансовый аналитик',
        ];
    }

    public function getPriceContent(): string
    {
        return '
<table class="table table-bordered table-striped">
    <tbody>
        <tr>
            <th>Название услуги/процедуры</th>
            <th>цена в рублях</th>
        </tr>
        <tr>
            <th colspan="2">Терапия</th>
        </tr>
        <tr>
            <td>Консультация, осмотр врача-стоматолога-терапевта</td>
            <td>бесплатно</td>
        </tr>
        <tr>
            <th colspan="2">Кариес</th>
        </tr>
        <tr>
            <td>Поверхностный кариес</td>
            <td>от 2 400</td>
        </tr>
        <tr>
            <td>Средний кариес</td>
            <td>от 2 850</td>
        </tr>
        <tr>
            <td>Глубокий кариес</td>
            <td>от 3 200</td>
        </tr>
        <tr>
            <td>Создание на поверхности зуба винирной облицовки</td>
            <td>от 5 000</td>
        </tr>
        <tr>
            <td>Пульпит</td>
            <td></td>
        </tr>
        <tr>
            <td>Одноканальный пульпит </td>
            <td>от 5 400</td>
        </tr>
        <tr>
            <td>Двухканальный пульпит </td>
            <td>от 6 800 </td>
        </tr>
        <tr>
            <td>Трехканальный пульпит</td>
            <td>от 8 300</td>
        </tr>
        <tr>
            <td>Четырехканальный пульпит</td>
            <td>от 9 300 </td>
        </tr>
        <tr>
            <th colspan="2">Периодонтит</th>
        </tr>
        <tr>
            <td>Одноканальный периодонтит</td>
            <td>от 6 100</td>
        </tr>
        <tr>
            <td>Двухканальный периодонтит</td>
            <td>от 7 100 </td>
        </tr>
        <tr>
            <td>Трехканальный периодонтит</td>
            <td>от 8 600</td>
        </tr>
        <tr>
            <td>Четырехканальный периодонтит</td>
            <td>от 9 600</td>
        </tr>
        <tr>
            <th colspan="2">Профессиональная гигиена</th>
        </tr>
        <tr>
            <td>Профессиональная гигиена полости рта и зубов</td>
            <td>2 300</td>
        </tr>
        <tr>
            <td>Профессиональная гигиена полости рта для детей до 18 лет </td>
            <td>1 600</td>
        </tr>
        <tr>
            <td>Профессиональная гигиена полости рта и зубов для ортодонтических пациентов</td>
            <td>2 000</td>
        </tr>
        <tr>
            <td>Профессиональная гигиена одного зуба</td>
            <td>250</td>
        </tr>
        <tr>
            <td>Отбеливание зубов - система Zoom (обе челюсти)</td>
            <td>14  000</td>
        </tr>
        <tr>
            <td>Домашнее отбеливание</td>
            <td>6 000</td>
        </tr>
        <tr>
            <td>Установка скайса (стразы)</td>
            <td>1 500</td>
        </tr>
        <tr>
            <td>Изготовление индивидуальной силиконовой капы для ремотерапии и отбеливания</td>
            <td>3 500</td>
        </tr>
        <tr>
            <th colspan="2">Ортодонтия</th>
        </tr>
        <tr>
            <td>Диагностика, первичный прием врача ортодонта (слепки, модели)</td>
            <td> 3 000 - 3 600</td>
        </tr>
        <tr>
            <td>Ортодонтическая коррекция съемными аппаратами</td>
            <td> 3 700 -10 000 </td>
        </tr>
        <tr>
            <td>Фиксация брекет системы металлической лигатурной (одна челюсть)</td>
            <td> 23 000-25 000</td>
        </tr>
        <tr>
            <td>Фиксация брекет системы металлической безлигатурной  (Damon) (одна челюсть)</td>
            <td>32 000-35 000</td>
        </tr>
        <tr>
            <td>Фиксация брекет системы керамческой лигатурной (одна челюсть)</td>
            <td>30 000-35 000</td>
        </tr>
        <tr>
            <td>Фиксация брекет системы керамической безлигатурной (Damon) (одна челюсть)</td>
            <td>35 000-40 000</td>
        </tr>
        <tr>
            <td>Установка ретенционного аппарата</td>
            <td>от 2 300</td>
        </tr>
        <tr>
            <td>Снятие брекет системы</td>
            <td>от 2 500</td>
        </tr>
        <tr>
            <td>Плановое посещение с активацией аппаратуры</td>
            <td>от 900</td>
        </tr>
        <tr>
            <td>Ортодонтическая коррекция несъемным ортодонтическим аппаратом</td>
            <td>10 300</td>
        </tr>
        <tr>
            <th colspan="2">РЕНТГЕН</th>
        </tr>
        <tr>
            <td>Прицельный снимок (снимок 1 зуба)  визиограф</td>
            <td>200 </td>
        </tr>
        <tr>
            <td>Компьютерная томография 3Д  1 зуба</td>
            <td>300</td>
        </tr>
        <tr>
            <td>Цефалостатичный снимок</td>
            <td>800 </td>
        </tr>
        <tr>
            <td>Снимок прямой/боковой проекции</td>
            <td>800 </td>
        </tr>
        <tr>
            <td>Компьютерная томография 3Д 1 сегмент</td>
            <td>750 </td>
        </tr>
        <tr>
            <td>Компьютерная томография 3Д</td>
            <td>2200 </td>
        </tr>
        <tr>
            <th colspan="2">ХИРУРГИЧЕСКОЕ ЛЕЧЕНИЕ</th>
        </tr>
        <tr>
            <td>Консультация, осмотр стоматолога-хирурга</td>
            <td>Бесплатно</td>
        </tr>
        <tr>
            <td>Анестезия</td>
            <td>300</td>
        </tr>
        <tr>
            <td>Удаление зуба простое </td>
            <td>1500</td>
        </tr>
        <tr>
            <td>Удаление зуба сложное </td>
            <td>от 2500</td>
        </tr>
        <tr>
            <td>Удаление зубов мудростей, ретинированных зубов</td>
            <td>3500-5500</td>
        </tr>
        <tr>
            <td>Удаление зубов мудрости на нижней челюсти</td>
            <td>5500-7500</td>
        </tr>
        <tr>
            <td>Синус – лифтинг </td>
            <td>20 000</td>
        </tr>
        <tr>
            <td>Направленная костная регенерация  </td>
            <td>20 000</td>
        </tr>
        <tr>
            <td>Имплантация «Под Ключ» - имплантат Osstem (Южная Корея) /формирователь десны/ абатмент / коронка</td>
            <td>39 000</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Имплантация «Под Ключ» - имплантат премиум класса Semados (Германия)  /формирователь десны/ абатмент / коронка</td>
            <td>75 000</td>
        </tr>
        <tr>
            <th colspan="2">ОРТОПЕДИЯ</th>
        </tr>
        <tr>
            <td>Консультация, осмотр стоматолога-ортопеда</td>
            <td>Бесплатно</td>
        </tr>
        <tr>
            <td>Анестезия</td>
            <td>300</td>
        </tr>
        <tr>
            <td>Металлокерамическая коронка</td>
            <td>6000</td>
        </tr>
        <tr>
            <td>Коронка из оксида циркония</td>
            <td>18000</td>
        </tr>
        <tr>
            <td>Коронка из оксида циркония на имплантат</td>
            <td>23000</td>
        </tr>
        <tr>
            <td>Коронка emax press</td>
            <td>16000</td>
        </tr>
        <tr>
            <td>Нанокоронка Varseo</td>
            <td>4000</td>
        </tr>
        <tr>
            <td>Нанокоронка Varseo c индивидуализацией композитом</td>
            <td>6000</td>
        </tr>
        <tr>
            <td>Нанокоронка Varseo на импланте</td>
            <td>13000</td>
        </tr>
        <tr>
            <td>Нанокоронка Varseo на импланте с индивидуализацией композитом</td>
            <td>15000</td>
        </tr>
        <tr>
            <td>Нановкладка Varseo</td>
            <td>4000</td>
        </tr>
        <tr>
            <td>Нановкладка Varseo с индивидуализацией</td>
            <td>6000</td>
        </tr>
        <tr>
            <td>Частично – съемный протез</td>
            <td>от 12000</td>
        </tr>
        <tr>
            <td>Бюгельный протез на замках</td>
            <td>45000</td>
        </tr>
        <tr>
            <td>Бюгельный протез на телескопических опорах</td>
            <td>45000</td>
        </tr>
        <tr>
            <td>Вкладка неразборная</td>
            <td>от 4000</td>
        </tr>
        <tr>
            <td>Временная пластмассовая коронка прямым методом</td>
            <td>1400</td>
        </tr>
        <tr>
            <td>Временная пластмассовая коронка лабораторным методом</td>
            <td>2400</td>
        </tr>
        <tr>
            <td>Временная пластмассовая коронка  cad/ cam</td>
            <td>3600</td>
        </tr>
        <tr>
            <td>Полный съемный протез по методу Гутовского</td>
            <td>30000</td>
        </tr>
        <tr>
            <td>Бюгельный протез классический</td>
            <td>26000</td>
        </tr>
        <tr>
            <td>Ацеталловый протез</td>
            <td>18000</td>
        </tr>
        <tr>
            <td>Частично-съемный протез с литым базисом с кламмерами из Acetal Dental</td>
            <td>30000</td>
        </tr>
        <tr>
            <td>Микропротез</td>
            <td>от 7000</td>
        </tr>
        <tr>
            <td>Полный съемный протез (1 челюсть)</td>
            <td>18000</td>
        </tr>
        <tr>
            <td>Керамические виниры и вкладки E.max press</td>
            <td>10000</td>
        </tr>
        <tr>
            <td>Мэриленд мост</td>
            <td>8000</td>
        </tr>
        <tr>
            <th colspan="2">ДЕТСКИЙ ПРИЕМ</th>
        </tr>
        <tr>
            <td>Консультация, осмотр стоматолога-терапевта</td>
            <td>бесплатно</td>
        </tr>
        <tr>
            <td>Анестезия</td>
            <td>250</td>
        </tr>
        <tr>
            <td>Лечение кариеса молочного зуба</td>
            <td>2000-2300</td>
        </tr>
        <tr>
            <td>Лечение кариеса постоянного зуба</td>
            <td>3000-3700</td>
        </tr>
        <tr>
            <td>Лечение пульпита молочного зуба</td>
            <td>от 2800</td>
        </tr>
        <tr>
            <td>Лечение пульпита постоянного зуба</td>
            <td>от 5000</td>
        </tr>
        <tr>
            <td>Лечение периодонтита молочного зуба</td>
            <td>от 3500</td>
        </tr>
        <tr>
            <td>Лечение периодонтита постоянного зуба</td>
            <td>6000-15000</td>
        </tr>
        <tr>
            <td>Восстановление зуба на штифте</td>
            <td>от 4000</td>
        </tr>
        <tr>
            <td>Герметизация (1 зуб)</td>
            <td>900</td>
        </tr>
        <tr>
            <td>Фторирование (1 зуб)</td>
            <td>150</td>
        </tr>
        <tr>
            <td>Медицинский осмотр </td>
            <td>350</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Удаление подвижного зуба (молочного)</td>
            <td>1100</td>
        </tr>
        <tr>
            <td>Удаление молочного зуба (с 6 лет)</td>
            <td>1550</td>
        </tr>
    </tbody>
</table>

        ';
    }
}