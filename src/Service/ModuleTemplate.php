<?php

namespace App\Service;

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\KernelInterface;
use Twig\Environment;

class ModuleTemplate
{
    private $kernel;
    private $twig;

    public function __construct(KernelInterface $kernel, Environment $twig)
    {
        $this->kernel = $kernel;
        $this->twig = $twig->getLoader();
    }

    public function getTemplate(object $module): string
    {
        if (null === $module) {
            throw new \Exception('Expecting Module object, null given');
        }

        if (null === $module->getName() || '' === $module->getName()) {
            throw new \Exception('Expecting string of name for define directory');
        }

        // dump($this->twig->exists('module/slider/default1.html.twig'));

        $templateName = $module->getTemplate() . '.html.twig';

        if ('' === $module->getTemplate()) { // give default template if empty field
            $templateName = 'default.html.twig';
        }

        $paths = ['prod/', ''];
        $moduleDir = 'module/';

        $templateWithPath = $moduleDir . $module->getName() . '/' . $templateName;

        $loadPath = '';

        foreach ($paths as $key => $value) {
            if ($this->twig->exists($value . $templateWithPath)) {
                $loadPath = $value . $templateWithPath;
            }
        }

        if (!empty($loadPath)) {
            return $loadPath;
        }

        throw new \Exception('Template with name ' . $templateWithPath . ' not found.');

        // $finder = new Finder();

        // $dirName = $this->kernel->getProjectDir() . '/templates/module/' . $module->getName();

        // $templateName = '/' . $module->getTemplate() . '.html.twig';

        // if ('' === $module->getTemplate()) { // give default template if empty field
        //     $templateName = '/default.html.twig';
        // }
        
        // $finder->in($dirName);

        // if ($finder->files()->name($dirName . $templateName)) {
        //     return 'module/' . $module->getName() . $templateName;
        // } else {
        //     throw new \Exception('Template with name ' . $templateName . ' not found.');
        // }

        // throw new \Exception($moduleDir . $module->getName() . ' directory does not exist.');
    }
}