<?php

namespace App\Service;

use App\Repository\SlugRepository as Slugs;
use App\Repository\PageRepository as Pages;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SlugRouter
{
    /**
     * @var string
     */
    public $slug;

    /**
     * @var Slug[]
     */
    public $items = [];

    /**
     * @var string
     */
    public $lastSlug;

    /**
     * @var Slug|null
     */
    public $findedSlug;

    /**
     * @var Page|null
     */
    public $landingPage;

    /**
     * @var string
     */
    public $forwardController;

    private $controllerDir;
    private $slugRepo;
    private $pageRepo;

    public function __construct(string $controllerDir, Slugs $slugs, Pages $pages)
    {
        $this->controllerDir = $controllerDir;
        $this->slugRepo = $slugs;
        $this->pageRepo = $pages;
    }

    public function find(string $slug): self
    {
        // dump($this);
        $this->slug = mb_strtolower(trim($slug), 'UTF-8');
        $parts = explode('/', $this->slug);
        $filter = array_filter($parts);

        if (1 === count($parts) && '' === $parts[0]) {
            $filter = $parts;
        }

        foreach ($filter as $key => $value) {
            $this->items[] = $this->slugRepo->findOneBySlug($value);
        }
        
        $lastSlug = end($this->items);

        if (null === $lastSlug) {
            throw new NotFoundHttpException();
        }

        $this->lastSlug = $lastSlug->getSlug();

        $this->findedSlug = $lastSlug;

        $this->pageDeterminant();

        $this->controllerDeterminant();

        if (false === $this->landingPage->getEnabled()) {
            throw new NotFoundHttpException();
        }

        return $this;
    }

    private function pageDeterminant()
    {
        if ($this->findedSlug->getRoute() === Pages::ROUTE) {

            $this->landingPage = $this->pageRepo->find($this->findedSlug->getEntityId());

        } else {

            // $pageSlug = $this->slugRepo->findOneBySlug($this->findedSlug->getRoute());
            $page = $this->pageRepo->findOneBy(['controller' => $this->findedSlug->getRoute()]);

            if (null !== $page) {
                $this->landingPage = $page;
            }
        }
    }

    private function controllerDeterminant()
    {
        $controller = ucfirst($this->landingPage->getController()) . 'Controller';

        // home page
        if (empty($this->slug)) {
            return;
        }

        $method = $this->findedSlug->getIsList() ? 'list' : 'show';

        // dynamic pages
        if (empty($this->landingPage->getController())) {
            $this->forwardController = 'App\\Controller\\PageController::' . $method;
            return;
        }

        // landing pages
        if (is_file($this->controllerDir . $controller . '.php')) {
            $this->forwardController = 'App\\Controller\\' . $controller . '::' . $method;
            return;
        }

        throw new \Exception('Cannot find controller file "' . $controller . '.php' . '"');
    }
}