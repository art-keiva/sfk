<?php

namespace App\Repository;

use App\Entity\Infoblock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Infoblock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Infoblock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Infoblock[]    findAll()
 * @method Infoblock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoblockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Infoblock::class);
    }

    // /**
    //  * @return Infoblock[] Returns an array of Infoblock objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Infoblock
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
