<?php

namespace App\Repository;

use App\Entity\Gtag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Gtag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gtag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gtag[]    findAll()
 * @method Gtag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GtagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gtag::class);
    }

    // /**
    //  * @return Gtag[] Returns an array of Gtag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Gtag
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
