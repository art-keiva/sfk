<?php

namespace App\Repository;

use App\Entity\Spcategory;
use Doctrine\Common\Persistence\ManagerRegistry;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * @method Spcategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method Spcategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method Spcategory[]    findAll()
 * @method Spcategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpcategoryRepository extends NestedTreeRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        $entityClass = Spcategory::class;
        $manager = $registry->getManagerForClass($entityClass);
        parent::__construct($manager, $manager->getClassMetadata($entityClass));
    }

    /**
     * @return Spcategory
     */
    public function findOneById(int $id): ?Spcategory
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $qb;
    }

    // /**
    //  * @return Spcategory[] Returns an array of Spcategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Spcategory
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
