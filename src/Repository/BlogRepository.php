<?php

namespace App\Repository;

use App\Entity\Blog;
use App\Entity\Bcategory as Category;
use App\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Blog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Blog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Blog[]    findAll()
 * @method Blog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Blog::class);
    }

    /**
     * @return Blog|null
     */
    public function findOneById(int $id): ?Blog
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $qb;
    }

    public function findLatest(int $page = 1, Category $category = null): Paginator
    {
        $qb = $this->createQueryBuilder('b')
            ->addSelect('c')
            ->leftJoin('b.bcategory', 'c')
            ->where('b.enabled = true')
            ->andWhere('b.publishedAt <= :now')
            ->orderBy('b.sort', 'ASC')
            ->addOrderBy('b.publishedAt', 'DESC')
            ->setParameter('now', new \DateTime())
        ;

        if (!is_null($category)) {
            $qb->andWhere(':bcategory = b.bcategory')
                ->setParameter('bcategory', $category);
        }

        return (new Paginator($qb, Blog::NUM_ITEMS))->paginate($page);
    }

    // /**
    //  * @return Blog[] Returns an array of Blog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Blog
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
