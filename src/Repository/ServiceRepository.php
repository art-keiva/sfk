<?php

namespace App\Repository;

use App\Entity\Service;
use App\Entity\Scategory as Category;
use App\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Service|null find($id, $lockMode = null, $lockVersion = null)
 * @method Service|null findOneBy(array $criteria, array $orderBy = null)
 * @method Service[]    findAll()
 * @method Service[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Service::class);
    }

    /**
     * @return Service|null
     */
    public function findOneById(int $id): ?Service
    {
        $qb = $this->createQueryBuilder('s')
            ->andWhere('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $qb;
    }

    public function findLatest(int $page = 1, Category $category = null): Paginator
    {
        $qb = $this->createQueryBuilder('s')
            ->addSelect('c')
            ->leftJoin('s.scategory', 'c')
            ->where('s.enabled = true')
            ->andWhere('s.createdAt <= :now')
            ->orderBy('s.sort', 'ASC')
            ->addOrderBy('s.createdAt', 'DESC')
            ->setParameter('now', new \DateTime())
        ;

        $qb->andWhere(':scategory MEMBER OF s.scategory')
            ->setParameter('scategory', $category);

        return (new Paginator($qb, Service::NUM_ITEMS))->paginate($page);
    }

    // /**
    //  * @return Service[] Returns an array of Service objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Service
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
