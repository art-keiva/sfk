<?php

namespace App\Repository;

use App\Entity\Ftag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ftag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ftag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ftag[]    findAll()
 * @method Ftag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FtagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ftag::class);
    }

    // /**
    //  * @return Ftag[] Returns an array of Ftag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ftag
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
