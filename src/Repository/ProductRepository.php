<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\Pcategory as Category;
use App\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Product|null
     */
    public function findOneById(int $id): ?Product
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $qb;
    }

    public function findLatest(int $page = 1, Category $category = null): Paginator
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->leftJoin('p.pcategory', 'c')
            ->where('p.enabled = true')
            ->andWhere('p.publishedAt <= :now')
            ->orderBy('p.sort', 'ASC')
            ->addOrderBy('p.publishedAt', 'DESC')
            ->setParameter('now', new \DateTime())
        ;

        if (!is_null($category)) {
            $qb->andWhere(':pcategory MEMBER OF p.pcategory')
                ->setParameter('pcategory', $category);
        }

        return (new Paginator($qb, Product::NUM_ITEMS))->paginate($page);
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
