<?php

namespace App\Repository;

use App\Entity\Pcategory;
use Doctrine\Common\Persistence\ManagerRegistry;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * @method Pcategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pcategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pcategory[]    findAll()
 * @method Pcategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PcategoryRepository extends NestedTreeRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        $entityClass = Pcategory::class;
        $manager = $registry->getManagerForClass($entityClass);
        parent::__construct($manager, $manager->getClassMetadata($entityClass));
    }

    /**
     * @return Pcategory
     */
    public function findOneById(int $id): ?Pcategory
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $qb;
    }

    // /**
    //  * @return Pcategory[] Returns an array of Pcategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pcategory
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
