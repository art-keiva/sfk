<?php

namespace App\Repository;

use App\Entity\Fileblock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Fileblock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fileblock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fileblock[]    findAll()
 * @method Fileblock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileblockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fileblock::class);
    }

    // /**
    //  * @return Fileblock[] Returns an array of Fileblock objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Fileblock
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
