<?php

namespace App\Repository;

use App\Entity\Bcategory;
use Doctrine\Common\Persistence\ManagerRegistry;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * @method Bcategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bcategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bcategory[]    findAll()
 * @method Bcategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BcategoryRepository extends NestedTreeRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        $entityClass = Bcategory::class;
        $manager = $registry->getManagerForClass($entityClass);
        parent::__construct($manager, $manager->getClassMetadata($entityClass));
    }

    /**
     * @return Bcategory
     */
    public function findOneById(int $id): ?Bcategory
    {
        $qb = $this->createQueryBuilder('b')
            ->andWhere('b.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $qb;
    }

    // /**
    //  * @return Bcategory[] Returns an array of Bcategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bcategory
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
