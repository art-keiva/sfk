<?php

namespace App\Repository;

use App\Entity\Specialist;
use App\Entity\Spcategory as Category;
use App\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Specialist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Specialist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Specialist[]    findAll()
 * @method Specialist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecialistRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Specialist::class);
    }

    /**
     * @return Specialist|null
     */
    public function findOneById(int $id): ?Specialist
    {
        $qb = $this->createQueryBuilder('s')
            ->andWhere('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $qb;
    }

    public function findLatest(int $page = 1, Category $category = null): Paginator
    {
        $qb = $this->createQueryBuilder('s')
            ->addSelect('c')
            ->leftJoin('s.spcategory', 'c')
            ->where('s.enabled = true')
            ->andWhere('s.createdAt <= :now')
            ->orderBy('s.sort', 'ASC')
            ->addOrderBy('s.createdAt', 'DESC')
            ->setParameter('now', new \DateTime())
        ;

        if (!is_null($category)) {
            $qb->andWhere(':spcategory MEMBER OF s.spcategory')
                ->setParameter('spcategory', $category);
        }

        return (new Paginator($qb, Specialist::NUM_ITEMS))->paginate($page);
    }

    // /**
    //  * @return Specialist[] Returns an array of Specialist objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Specialist
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
