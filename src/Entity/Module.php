<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ModuleRepository")
 * @ORM\Table(name="module")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Module
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="media_images", fileNameProperty="image")
     * @Assert\File(
     *     maxSize = "5m",
     *     maxSizeMessage = "image.too_big",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/png", "image/gif"},
     *     mimeTypesMessage = "image.valid_mime_type",
     * )
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $background;

    /**
     * @Vich\UploadableField(mapping="media_images", fileNameProperty="background")
     * @Assert\File(
     *     maxSize = "5m",
     *     maxSizeMessage = "image.too_big",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/png", "image/gif"},
     *     mimeTypesMessage = "image.valid_mime_type",
     * )
     * @var File
     */
    private $backgroundFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $template;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="integer")
     */
    private $width;

    /**
     * @ORM\Column(type="integer")
     */
    private $height;

    /**
     * @ORM\Column(type="integer")
     */
    private $width2;

    /**
     * @ORM\Column(type="integer")
     */
    private $height2;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Page", mappedBy="module")
     */
    private $page;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Infoblock", mappedBy="module")
     */
    private $infoblock;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Fileblock", mappedBy="module")
     */
    private $fileblock;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Slider", mappedBy="module")
     */
    private $slider;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contact", mappedBy="module")
     */
    private $contact;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->page = new ArrayCollection();
        $this->infoblock = new ArrayCollection();
        $this->fileblock = new ArrayCollection();
        $this->slider = new ArrayCollection();
        $this->contact = new ArrayCollection();
        $this->updatedTimestamps();
        $this->setImage('');
        $this->setBackground('');
        $this->setWidth(0);
        $this->setHeight(0);
        $this->setWidth2(0);
        $this->setHeight2(0);
        $this->setEnabled(true);
    }

    public function __toString()
    {
        return $this->title; 
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param File $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image) {
            $this->setUpdatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = (string) $image;

        return $this;
    }

    /**
     * @param File $background
     */
    public function setBackgroundFile(File $background = null)
    {
        $this->backgroundFile = $background;
        if ($background) {
            $this->setUpdatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return File
     */
    public function getBackgroundFile()
    {
        return $this->backgroundFile;
    }

    public function getBackground(): ?string
    {
        return $this->background;
    }

    public function setBackground(?string $background): self
    {
        $this->background = (string) $background;

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(?string $template): self
    {
        $this->template = (string) $template;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth2(): ?int
    {
        return $this->width2;
    }

    public function setWidth2(int $width2): self
    {
        $this->width2 = $width2;

        return $this;
    }

    public function getHeight2(): ?int
    {
        return $this->height2;
    }

    public function setHeight2(int $height2): self
    {
        $this->height2 = $height2;

        return $this;
    }

    /**
     * @return Collection|Page[]
     */
    public function getPage(): Collection
    {
        return $this->page;
    }

    public function addPage(Page $page): self
    {
        if (!$this->page->contains($page)) {
            $this->page[] = $page;
            $page->addModule($this);
        }

        return $this;
    }

    public function removePage(Page $page): self
    {
        if ($this->page->contains($page)) {
            $this->page->removeElement($page);
            $page->removeModule($this);
        }

        return $this;
    }

    /**
     * @return Collection|Infoblock[]
     */
    public function getInfoblock(): Collection
    {
        return $this->infoblock;
    }

    public function addInfoblock(Infoblock $infoblock): self
    {
        if (!$this->infoblock->contains($infoblock)) {
            $this->infoblock[] = $infoblock;
            $infoblock->setModule($this);
        }

        return $this;
    }

    public function removeInfoblock(Infoblock $infoblock): self
    {
        if ($this->infoblock->contains($infoblock)) {
            $this->infoblock->removeElement($infoblock);
            // set the owning side to null (unless already changed)
            if ($infoblock->getModule() === $this) {
                $infoblock->setModule(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Fileblock[]
     */
    public function getFileblock(): Collection
    {
        return $this->fileblock;
    }

    public function addFileblock(Fileblock $fileblock): self
    {
        if (!$this->fileblock->contains($fileblock)) {
            $this->fileblock[] = $fileblock;
            $fileblock->setModule($this);
        }

        return $this;
    }

    public function removeFileblock(Fileblock $fileblock): self
    {
        if ($this->fileblock->contains($fileblock)) {
            $this->fileblock->removeElement($fileblock);
            // set the owning side to null (unless already changed)
            if ($fileblock->getModule() === $this) {
                $fileblock->setModule(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Slider[]
     */
    public function getSlider(): Collection
    {
        return $this->slider;
    }

    public function addSlider(Slider $slider): self
    {
        if (!$this->slider->contains($slider)) {
            $this->slider[] = $slider;
            $slider->setModule($this);
        }

        return $this;
    }

    public function removeSlider(Slider $slider): self
    {
        if ($this->slider->contains($slider)) {
            $this->slider->removeElement($slider);
            // set the owning side to null (unless already changed)
            if ($slider->getModule() === $this) {
                $slider->setModule(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContact(): Collection
    {
        return $this->contact;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contact->contains($contact)) {
            $this->contact[] = $contact;
            $contact->setModule($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contact->contains($contact)) {
            $this->contact->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getModule() === $this) {
                $contact->setModule(null);
            }
        }

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
        $this->setUpdatedAt(new \DateTime('now'));
    }
}
