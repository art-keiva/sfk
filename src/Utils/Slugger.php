<?php

namespace App\Utils;

use App\Repository\SlugRepository as Slugs;
use Behat\Transliterator\Transliterator as Transliterator;

class Slugger
{
	public $separator = '-';
	public $slugRepo;

    public function __construct(Slugs $slugs)
    {
        $this->slugRepo = $slugs;
    }

    public function slugify(string $string, ?int $id = null): string
    {
        $transTitle = Transliterator::transliterate($string, $this->separator);
        $urlizedTitle = Transliterator::urlize($transTitle, $this->separator);

        $string = $this->getUniqueSlug($urlizedTitle, $id);

    	return $string;
    }

    public function getUniqueSlug(string $string, ?int $id = null): ?string
    {
    	$findedSlug = $this->slugRepo->findOneBySlug($string);

    	if (!is_null($findedSlug) && $findedSlug->getId() !== $id) {
			$i = 1;
    		$exploded = explode('-', $string);
    		$trail = end($exploded);

            // find the previous generated trail
            if (is_numeric($trail) && count($exploded) !== 1) {
                $taken = array_pop($exploded);
                $i = $i + (int) $taken;
            }

    		foreach ($exploded as $key => $value) {
    			if ($key === 0) {
    				$string = $value;
    			} else {
    				$string .= $this->separator . $value;
    			}
    		}

			$newUrlizedTitle = $string . $this->separator . $i;

    		$string = $this->getUniqueSlug($newUrlizedTitle);
    	}

    	return $string;
    }

    public function hasMethodsAndConsts(string $class): bool
    {
        if (!method_exists($class, 'getSlug')) {
            return false;
        }

        if (!defined("$class::ROUTE") && !defined("$class::IS_LIST")) {
            return false;
        }

        return true;
    }
}
