<?php

namespace App\Utils;

use App\Repository\InfoRepository;

class Info
{
    public function __construct(InfoRepository $info)
    {
        $items = $info->findAll();

        foreach ($items as $key => $value) {
            $this->{$value->getKey()}[$value->getName()] = $value->getValue();
        }
    }
}
