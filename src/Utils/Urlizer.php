<?php

namespace App\Utils;

use Symfony\Component\HttpFoundation\UrlHelper;

class Urlizer
{
    private $urlHelper;

    public function __construct(UrlHelper $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    public function url(array $path = [], bool $absolute = true): string
    {
        $url = '';
        $break = false;

        foreach ($path as $key => $value) {
            $pos1 = strpos($value, '?');
            if (false !== $pos1) {
                $value = substr($value, 0, $pos1);
                $break = true;
            }

            $pos2 = strpos($value, '#');
            if (false !== $pos2) {
                $value = substr($value, 0, $pos2);
                $break = true;
            }

            if ('/' === substr($value, 0, 1)) {
                $url .= $value;
            } else {
                $url .= '/' . $value;
            }

            if ($break) {
                break;
            }
        }

        if ($absolute) {
            $url = $this->urlHelper->getAbsoluteUrl($url);
            if ('/' === substr($url, -1)) {
                $url = substr($url, 0, -1);
            }
        }

        return $url;
    }

    public function base(string $path = 'removeme'): string
    {
        $url = $this->urlHelper->getAbsoluteUrl($path);

        $exploded = explode('/', $url);

        $url = '';

        foreach ($exploded as $key => $value) {
            if ($key < 3) {
                if ($key === 1) {
                    $url .= '//';
                }
                
                $url .= $value;
            }
        }


        return $url; // hopelessness
    }
}
