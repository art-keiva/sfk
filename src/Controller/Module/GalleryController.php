<?php

namespace App\Controller\Module;

use App\Repository\GtagRepository as Categories;
use App\Repository\GalleryRepository as Galleries;
use App\Service\ModuleTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GalleryController extends AbstractController
{
    public function index(Request $request, Categories $categories, Galleries $galleries, ModuleTemplate $template): Response
    {
    	$slugRouter = $request->get('slug_router');
    	$module = $request->get('module');

        return $this->render($template->getTemplate($module), [
            'slug_router' => $slugRouter,
            'module' => $module,
            'categories' => $categories->findBy([], ['sort' => 'ASC', 'id' => 'DESC']),
            'items' => $galleries->findBy(['enabled' => true, 'special' => true, ], ['sort' => 'ASC', 'createdAt' => 'DESC']),
        ]);
    }
}