<?php

namespace App\Controller\Module;

use App\Repository\ProductRepository as Products;
use App\Service\ModuleTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    public function index(Request $request, Products $products, ModuleTemplate $template): Response
    {
    	$slugRouter = $request->get('slug_router');
    	$module = $request->get('module');

        return $this->render($template->getTemplate($module), [
            'slug_router' => $slugRouter,
            'module' => $module,
            'items' => $products->findBy(['enabled' => true, 'special' => true, ], ['sort' => 'ASC', 'createdAt' => 'DESC']),
        ]);
    }
}