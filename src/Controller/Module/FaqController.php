<?php

namespace App\Controller\Module;

use App\Repository\FtagRepository as Categories;
use App\Repository\FaqRepository as Faqs;
use App\Service\ModuleTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FaqController extends AbstractController
{
    public function index(Request $request, Categories $categories, Faqs $faqs, ModuleTemplate $template): Response
    {
    	$slugRouter = $request->get('slug_router');
    	$module = $request->get('module');

        return $this->render($template->getTemplate($module), [
            'slug_router' => $slugRouter,
            'module' => $module,
            'categories' => $categories->findBy([], ['sort' => 'ASC', 'id' => 'DESC']),
            'items' => $faqs->findBy(['enabled' => true, 'special' => true, ], ['sort' => 'ASC', 'createdAt' => 'DESC']),
        ]);
    }
}