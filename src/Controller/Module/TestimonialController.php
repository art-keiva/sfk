<?php

namespace App\Controller\Module;

use App\Repository\PageRepository as Pages;
use App\Repository\TestimonialRepository as Testimonials;
use App\Service\ModuleTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TestimonialController extends AbstractController
{
    public function index(Request $request, Testimonials $testimonials, ModuleTemplate $template, Pages $pages): Response
    {
    	$slugRouter = $request->get('slug_router');
    	$module = $request->get('module');

        $entityPage = $pages->findOneBy(['controller' => $module->getName()]);

        return $this->render($template->getTemplate($module), [
            'slug_router' => $slugRouter,
            'module' => $module,
            'items' => $testimonials->findBy(['enabled' => true, 'special' => true, ], ['sort' => 'ASC', 'createdAt' => 'DESC']),
            'link' => (null !== $entityPage) ? $entityPage->getSlug() : null,
        ]);
    }
}