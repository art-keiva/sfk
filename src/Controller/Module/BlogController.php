<?php

namespace App\Controller\Module;

use App\Repository\PageRepository as Pages;
use App\Repository\BlogRepository as Blogs;
use App\Service\ModuleTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    public function index(Request $request, Blogs $blogs, ModuleTemplate $template, Pages $pages): Response
    {
    	$slugRouter = $request->get('slug_router');
    	$module = $request->get('module');

        return $this->render($template->getTemplate($module), [
            'slug_router' => $slugRouter,
            'module' => $module,
            'items' => $blogs->findBy(['enabled' => true, 'special' => true, ], ['sort' => 'ASC', 'createdAt' => 'DESC']),
            'link' => $pages->findOneBy(['controller' => $module->getName()])->getSlug() ?? null,
        ]);
    }
}