<?php

namespace App\Controller\Module;

use App\Repository\BcategoryRepository as Bcategories;
use App\Repository\PcategoryRepository as Pcategories;
use App\Repository\ScategoryRepository as Scategories;
use App\Service\ModuleTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategoryController extends AbstractController
{
    private $pcategories;
    private $bcategories;
    private $scategories;

    public function __construct(Pcategories $pcategories, Bcategories $bcategories, Scategories $scategories)
    {
        $this->pcategories = $pcategories;
        $this->bcategories = $bcategories;
        $this->scategories = $scategories;
    }

    public function index(Request $request, ModuleTemplate $template): Response
    {
    	$slugRouter = $request->get('slug_router');

        switch ($slugRouter->landingPage->getController()) {
            case 'service':
                $categories = $this->scategories;
                break;
            case 'blog':
                $categories = $this->bcategories;
                break;
            
            default:
                $categories = $this->pcategories;
                break;
        }

    	$module = $request->get('module');

        return $this->render($template->getTemplate($module), [
            'children' => $categories->getChildren(null, true, 'sort'),
            'slug_router' => $slugRouter,
            'module' => $module,
        ]);
    }
}