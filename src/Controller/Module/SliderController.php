<?php

namespace App\Controller\Module;

use App\Repository\SliderRepository;
use App\Service\ModuleTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SliderController extends AbstractController
{
    public function index(Request $request, SliderRepository $sliderRepo, ModuleTemplate $template): Response
    {
    	$slugRouter = $request->get('slug_router');
    	$module = $request->get('module');
    	$args = $request->get('args');

        $parameters = [
            'slug_router'   => $slugRouter,
            'module'        => $module,
            'args'        => $args,
        ];

        return $this->render($template->getTemplate($module), $parameters);
    }
}