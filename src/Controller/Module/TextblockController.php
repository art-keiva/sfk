<?php

namespace App\Controller\Module;

use App\Repository\InfoblockRepository;
use App\Service\ModuleTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TextblockController extends AbstractController
{
    public function index(Request $request, ModuleTemplate $template): Response
    {
    	$slugRouter = $request->get('slug_router');
    	$module = $request->get('module');
        $args = $request->get('args');

        $parameters = [
            'slug_router'   => $slugRouter,
            'module'        => $module,
        ];

        if (null !== $args) {
            if (isset($args['form'])) {
                $parameters['form'] = $args['form'];
            }
            if (isset($args['page_terms'])) {
                $parameters['page_terms'] = $args['page_terms'];
            }
        }

        return $this->render($template->getTemplate($module), $parameters);
    }
}