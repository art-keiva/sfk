<?php

namespace App\Controller\Module;

use App\Repository\InfoblockRepository;
use App\Service\ModuleTemplate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class InfoblockController extends AbstractController
{
    public function index(Request $request, InfoblockRepository $infoblockRepo, ModuleTemplate $template): Response
    {
    	$slugRouter = $request->get('slug_router');
    	$module = $request->get('module');

        return $this->render($template->getTemplate($module), [
            'slug_router' => $slugRouter,
            'module' => $module,
        ]);
    }
}