<?php

namespace App\Controller;

use App\Entity\Page;
use App\Repository\PcategoryRepository as Pcategories;
use App\Repository\ProductRepository as Products;
use App\Repository\PageRepository as Pages;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\Urlizer;
use App\Utils\Info;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\NamedAddress;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class CatalogController extends AbstractController
{

    public function list(Request $request, Pcategories $categories, Products $products, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');
        $category = null;
        $stackSlugs = [];

        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }

            $category = $categories->findOneById($value->getEntityId());

            if (null !== $category) {
                $stackSlugs[] = $category->getSlug();
                $breadcrumbs->addItem($category->getTitle(), $url->url($stackSlugs));
            }
        }

        $request->query = $request->attributes->get('query') ?? $request->query;

        return $this->render('catalog/list.html.twig', [
            'page'              => $category ?? $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'categories'        => $categories->getChildren($category, true, 'sort'),
            'paginator'         => $products->findLatest($request->query->get('page') ?? 1, $category),
        ]);
    }

    public function show(Request $request, Pcategories $categories, Products $products, Breadcrumbs $breadcrumbs, Urlizer $url, Info $info, MailerInterface $mailer, Pages $pages): Response
    {
        $slugRouter = $request->get('slug_router');

        $item = $products->find($slugRouter->findedSlug->getEntityId());
        
        $stackSlugs = [];
        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }
            
            if ($value->getIsList()) {
                $category = $categories->findOneById($value->getEntityId());

                if (null !== $category) {
                    $stackSlugs[] = $category->getSlug();
                    $breadcrumbs->addItem($category->getTitle(), $url->url($stackSlugs));
                }
            }
        }

        $stackSlugs[] = $item->getSlug();
        $breadcrumbs->addItem($item->getTitle(), $url->url($stackSlugs));



        // order form
        $formBuilder = $this->createFormBuilder([], [
            'csrf_protection' => true,
            // 'allow_extra_fields' => true,
        ]);

        $formBuilder
            ->add('quantity', IntegerType::class, [
                'label' => 'label.order.quantity',
                'data' => 1,
                'data_class' => null,
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new Assert\Positive(),
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.order.email',
                'data_class' => null,
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email([
                        // 'message' => 'The email "{{ value }}" is not a valid email.',
                        // 'message' => 'error.valid.email',
                    ]),
                ],
            ])
            ->add('message', TextAreaType::class, [
                'label' => 'label.order.message',
                'data_class' => null,
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new Assert\Length([
                        'max' => 65535,
                    ]),
                ],
                'invalid_message' => 'error.valid.message',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'label.order'
            ])
        ;

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailData = (new TemplatedEmail())
                ->from(new NamedAddress($this->getParameter('mailer.name'), $info->project['sitename']))
                // ->to('company@gmail.com')
                ->to(new Address($info->project['email']))
                //->cc('cc@example.com')
                //->bcc('bcc@example.com')
                //->replyTo('fabien@example.com')
                //->priority(Email::PRIORITY_HIGH)
                ->subject('Заказ товара, от ' . $form->get('email')->getData())
                // ->text('Sending emails is fun again!')
                // ->html($emailContent)
                ->htmlTemplate('emails/catalog.order.html.twig')
                ->context([
                    'userProductName' => $item->getTitle(),
                    'userProductLink' => $request->getUri(),
                    'userQuantity' => $form->get('quantity')->getData(),
                    'userEmail' => $form->get('email')->getData(),
                    'userMessage' => $form->get('message')->getData(),
                ])
            ;

            try {
                $mailer->send($emailData);
            } catch (Exception $e) {
                throw new \Exception('Failed to send message.');
            }

            $this->addFlash('success', 'label.mailer.success');

            return $this->redirect($request->getUri());
        }

        return $this->render('catalog/show.html.twig', [
            'page'              => $item ?? $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'item'              => $item,
            'form'              => $form->createView(),
            'page_terms'        => $pages->findOneBy(['controller' => 'terms']),
        ]);
    }
}
