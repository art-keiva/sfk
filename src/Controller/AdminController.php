<?php

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends EasyAdminController
{
    public function indexAction(Request $request): Response
    {
        parent::initialize($request);

        // return parent::redirectToBackendHomepage();

        return $this->render('admin/default/index.html.twig', [
        ]);
    }
}
