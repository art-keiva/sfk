<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;

class UserController extends EasyAdminController
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    
    protected function editAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_EDIT);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        if ($this->request->isXmlHttpRequest() && $property = $this->request->query->get('property')) {
            $newValue = 'true' === mb_strtolower($this->request->query->get('newValue'));
            $fieldsMetadata = $this->entity['list']['fields'];

            if (!isset($fieldsMetadata[$property]) || 'toggle' !== $fieldsMetadata[$property]['dataType']) {
                throw new \RuntimeException(sprintf('The type of the "%s" property is not "toggle".', $property));
            }

            $this->updateEntityProperty($entity, $property, $newValue);

            // cast to integer instead of string to avoid sending empty responses for 'false'
            return new Response((int) $newValue);
        }

        $fields = $this->entity['edit']['fields'];

        $editForm = $this->executeDynamicMethod('createEditForm', [$entity, $fields]);
        $deleteForm = $this->createDeleteForm($this->entity['name'], $id);

        $savePreviousPassword = $entity->getPassword();

        // $this->request->request->get('user')['password'];

        $editForm->handleRequest($this->request); // set entity values from edit form!

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->processUploadedFiles($editForm);

            $postedPassword = $entity->getPassword();
            if (empty($postedPassword)) {
                $entity->setPassword($savePreviousPassword);
            } else {
                $entity->setPassword($this->passwordEncoder->encodePassword($entity, $postedPassword));
            }

            $this->dispatch(EasyAdminEvents::PRE_UPDATE, ['entity' => $entity]);
            $this->executeDynamicMethod('updateEntity', [$entity, $editForm]);
            $this->dispatch(EasyAdminEvents::POST_UPDATE, ['entity' => $entity]);

            return $this->redirectToReferrer();
        }

        $this->dispatch(EasyAdminEvents::POST_EDIT);

        $parameters = [
            'form' => $editForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ];

        return $this->executeDynamicMethod('renderTemplate', ['edit', $this->entity['templates']['edit'], $parameters]);
    }

    /*protected function updateEntity($entity) {

        // $user = $this->em->getRepository(User::class)->find($entity->getId());

        // dump($user);
        // dump($entity);
        // dump($qwe);

        //-------------
        $entity->setFullname($entity->getFullname());
        $entity->setUsername($entity->getUsername());
        $entity->setEmail($entity->getEmail());

        $postedPassword = $entity->getPassword();
        if (!empty($postedPassword))
        {
            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $postedPassword));
        }

        $entity->setRoles($entity->getRoles());

        $this->em->persist($entity);
        $this->em->flush();
    }*/

    // protected function getEntityFormOptions($entity, $view);


    // protected function updateUserEntity($entity)
    // {

        // if (method_exists($entity, 'fullname')) {
        //     $entity->setUpdatedAt(new \DateTime());
        // }

        // $user = $this->request->get('user'); // array
        // $user = $this->em->getRepository(User::class)->find($entity->getId());

        // dump($user);
        // dump($entity);
        // dump($user->getPassword());

        // dump($qwe);


        // $id = $this->request->query->get('id');
        
        /*dump($entity);
        $this->dispatch(EasyAdminEvents::PRE_EDIT);

        dump($entity);
        dump($asd);

        $this->dispatch(EasyAdminEvents::POST_EDIT);

        dump($asd);


        $user = $this->em->getRepository(User::class)->find($entity->getId());
        // $entity->setFullName('qwe111');

        $user->setFullname($entity->getFullname());
        $user->setUsername($entity->getUsername());
        $user->setEmail($entity->getEmail());

        $postedPassword = $entity->getPassword();
        if (!empty($postedPassword))
        {
            $user->setPassword($this->passwordEncoder->encodePassword($user, $postedPassword));
        }

        $user->setRoles($entity->getRoles());

        $this->em->persist($user);
        $this->em->flush();

        // parent::updateEntity($entity);

        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));*/



        // $query = $request->query->all();
        // dump($this->entity['name']);
        // dump($this->request);
        // dump($entity);
        // dump($em);
        // dump($this);
        // $entity->fullname = 'qwe';
        // $entity->setFullName($entity->fullname);
        // $user1->setUsername($username);
        // $entity->setPassword($this->passwordEncoder->encodePassword($user, $password));
        // $entity->password = $this->passwordEncoder->encodePassword($entity, $password);
        // $user1->setEmail($email);

        // $em->persist($entity);
        // $em->flush();


        // $this->passwordEncoder->encodePassword($user, $password);
    // }

    // protected function editAction(Response $response): Response
    // {
    //     dump(111);
    // }








    /*
    protected function prePersistUserEntity(User $user)
    {
    	// dump(111);
        // $encodedPassword = $this->encodePassword($user, $user->getPassword());
        // $user->setPassword($encodedPassword);
    }

    protected function preUpdateUserEntity(User $user)
    {
    	// dump(111);
        // if (!$user->getPlainPassword()) {
        //     return;
        // }
        // $encodedPassword = $this->encodePassword($user, $user->getPlainPassword());
        // $user->setPassword($encodedPassword);
    }

    private function encodePassword($user, $password)
    {
    	// dump(111);
        // $passwordEncoderFactory = $this->get('security.encoder_factory');
        // $encoder = $passwordEncoderFactory->getEncoder($user);
        // return $encoder->encodePassword($password, $user->getSalt());
    }

    protected function showUserAction()
    {
    	// dump(111);

    }

    protected function createNewUserEntity()
    {
    	// dump(111);
    }
    */
}
