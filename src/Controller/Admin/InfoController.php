<?php

namespace App\Controller\Admin;

use App\Entity\Info;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
// use Vich\UploaderBundle\Form\Type\VichFileType;
// use Vich\UploaderBundle\Form\Type\VichImageType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

// use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Routing\Annotation\Route;

// use Behat\Transliterator\Transliterator;
use Symfony\Contracts\Translation\TranslatorInterface;


class InfoController extends AbstractController
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * List of all info rows.
     *
     * @Route("/admin/info", methods={"GET", "POST"}, name="admin_info_index")
     */
    public function index(Request $request, FileUploader $fileUploader)
    {
        $em = $this->getDoctrine()->getManager();

        $infoResults = $em->getRepository(Info::class)->findAll();

        $info = [];

        foreach ($infoResults as $key => $value) {
        	$info[$value->getKey()][$value->getName()] = $value->getValue();
        }


        $imagesType = ['icon', 'logo'];
        $textareaType = ['address'];
        $textareaEditorType = ['comment'];
        $checkboxType = ['mode'];

        // $defaultData = ['sitename' => ''];
        $defaultData = [];

	    // $form = $this->createFormBuilder($defaultData);
	    $formBuilder = $this->createFormBuilder($defaultData, [
            'csrf_protection' => true,
            'allow_extra_fields' => true,
        ]);


        foreach ($info as $key => $value) {
        	// $info[$value->getKey()][$value->getName()] = $value->getValue();
        	
        	foreach ($value as $key2 => $value2) {

		        // Text
		        $fieldType = TextType::class;

		        // Images
		        if (in_array($key2, $imagesType)) {
		            $fieldType = FileType::class;
		        }

		        // Textarea
		        if (in_array($key2, $textareaType)) {
		            $fieldType = TextAreaType::class;
		        }

		        // Textarea with editor
		        if (in_array($key2, $textareaEditorType)) {
		            $fieldType = CKEditorType::class;
		        }

                // Textarea with editor
                $exploded = explode('_', $key2);
                if ($exploded[0] === $checkboxType[0]) {
                    $fieldType = CheckboxType::class;
                }



                $fieldOptions = [
                    'label' => $this->translator->trans('label.' . $key . '.' . $key2, [], 'admin'),
                    // 'label' => 'label.' . $key . '.' . $key2,
                    'data' => $value2,
                    'data_class' => null,
                    'mapped' => false,
                    'required' => false,
                    'allow_extra_fields' => true,
                    'attr' => [
                        'data_parent' => $key,
                    ],
                ];

                if (in_array($key2, $imagesType)) {
                    $fieldOptions['constraints'] = [
                        new File([
                            'maxSize' => '5m',
                            'maxSizeMessage' => 'image.too_big',
                            'mimeTypes' => [
                                'image/jpg',
                                'image/jpeg',
                                'image/png',
                                'image/gif',
                            ],
                            'mimeTypesMessage' => 'image.valid_mime_type',
                        ])
                    ];
                }

                if ($exploded[0] === $checkboxType[0]) {
                    settype($value2, 'integer');
                    $fieldOptions['data'] = (boolean) $value2;
                    $fieldOptions['label'] = 'label.mode.imagine';
                }


        		
        		$formBuilder
        			->add($key . '-' . $key2, $fieldType, $fieldOptions);

                // if ($value2 && in_array($key2, $imagesType)) {
                //     $formBuilder
                //         ->add($key . '-' . $key2 . '-delete', CheckboxType::class, [
                //             'label' => 'delete',
                //             'required' => false,
                //             'mapped' => false,
                //         ]);
                // }
        	}

        }

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = [];
            // $data2 = $form->get('project-email')->getData();

            foreach ($info as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $fieldName = $key2;
                    $fieldKeyName = $key . '-' . $key2;
                    $fieldData = $form->get($fieldKeyName)->getData();

                    if ($fieldData instanceof UploadedFile) {
                        if (in_array($fieldName, $imagesType)) {
                            $fieldData = $fileUploader->upload($fieldData);
                        } else {
                            $fieldData = $fileUploader->upload($fieldData, false);
                        }
                    }

                    $fieldValue = trim($fieldData);

                    $data[$key . '-' . $key2] = $fieldValue;
                }
            }

            // dump($form);
            // dump($data);
            // dump($data2);
            // dump($form->getExtraData());

            foreach ($form->getExtraData() as $key2 => $value2) {
                if ($key2 === 'delete') {
                    foreach ($value2 as $key3 => $value3) {
                        $exploded = explode('-', trim($key3));
                        $fieldKey = $exploded[0];
                        $fieldName = $exploded[1];
                        
                        foreach ($infoResults as $key4 => $value4) {
                            if ($value4->getKey() === $fieldKey && $value4->getName() === $fieldName) {
                                if ($fileUploader->remove($value4->getValue())) {
                                    $value4->setValue('');
                                    $em->persist($value4);
                                }
                            }
                        }
                    }
                }
            }

            $em->flush();

        	foreach ($data as $key => $value) {
                $exploded = explode('-', trim($key));
                $fieldKey = $exploded[0];
                $fieldName = $exploded[1];
                $fieldValue = trim($value); // convert to empty string if value is null
                
                // dump($fieldKey . ' - ' . $fieldName . ' - ' . $fieldValue);
                
                foreach ($infoResults as $key2 => $value2) {
                    if ($value2->getKey() === $fieldKey && $value2->getName() === $fieldName) {
                        if (in_array($fieldName, $imagesType) && empty($value)) {
                            continue;
                        }

                        if (null !== $value) {
                            $value2->setValue($fieldValue);
                            $em->persist($value2);
                        }
                    }
                }
        	}

            $em->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/book/controller.html#flash-messages
            $this->addFlash('success', $this->translator->trans('label.success', [], 'admin'));


            return $this->redirectToRoute('admin_info_index', [
                'menuIndex' => $request->query->get('menuIndex'),
                'submenuIndex' => $request->query->get('submenuIndex'),
            ]);
        }

        return $this->render('admin/info/edit.html.twig', [
        	'form' => $form->createView(),
        ]);
    }
}