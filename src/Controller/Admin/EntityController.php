<?php

namespace App\Controller\Admin;

// use App\Entity\Slug;
// use App\Repository\SlugRepository;
// use App\Utils\Slugger;
// use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\HttpFoundation\Response;
// // use Doctrine\Common\Persistence\ObjectManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
// use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;


class EntityController extends EasyAdminController
{
    // private $slugRepository;
    // private $slugger;

    // public function __construct(SlugRepository $slugRepository, Slugger $slugger)
    // {
    //     $this->slugRepository = $slugRepository;
    //     $this->slugger = $slugger;
    // }

/*
    protected function newAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_NEW);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        $entity = $this->executeDynamicMethod('createNewEntity');

        $easyadmin = $this->request->attributes->get('easyadmin');
        $easyadmin['item'] = $entity;
        $this->request->attributes->set('easyadmin', $easyadmin);

        $fields = $this->entity['new']['fields'];
        $newForm = $this->executeDynamicMethod('createNewForm', [$entity, $fields]);

        $newForm->handleRequest($this->request);
        if ($newForm->isSubmitted() && $newForm->isValid()) {
            $this->processUploadedFiles($newForm);

            // save the slug part 1
            // if (is_null($entity->getSlug())) {
            //     $entity->setSlug($entity->getTitle());
            // }
            // $slugifiedTitle = $this->slugger->slugify($entity->getSlug());
            // $entity->setSlug($slugifiedTitle);

            $this->dispatch(EasyAdminEvents::PRE_PERSIST, ['entity' => $entity]);
            $this->executeDynamicMethod('persistEntity', [$entity, $newForm]);
            $this->dispatch(EasyAdminEvents::POST_PERSIST, ['entity' => $entity]);
            
            // save the slug part 2
            // $em = $this->getDoctrine()->getManager();
            // $newSlug = new Slug();
            // $newSlug->setRoute(Product::ROUTE);
            // $newSlug->setEntityId($entity->getId());
            // $newSlug->setIsList(Product::IS_LIST);
            // $newSlug->setSlug($entity->getSlug());

            // $em->persist($newSlug);
            // $em->flush();
            // die;
            return $this->redirectToReferrer();
        }

        $this->dispatch(EasyAdminEvents::POST_NEW, [
            'entity_fields' => $fields,
            'form' => $newForm,
            'entity' => $entity,
        ]);

        $parameters = [
            'form' => $newForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
        ];

        return $this->executeDynamicMethod('renderTemplate', ['new', $this->entity['templates']['new'], $parameters]);
    }
*/
    
    // protected function editAction()
    // {
    //     $this->dispatch(EasyAdminEvents::PRE_EDIT);

    //     $id = $this->request->query->get('id');
    //     $easyadmin = $this->request->attributes->get('easyadmin');
    //     $entity = $easyadmin['item'];
    //     $entityClass = $this->entity['class'];

    //     // save the slug part 1
    //     if (method_exists($entityClass, 'getSlug')) {
    //         if (!defined("$entityClass::ROUTE")) {
    //             throw new \Exception('The class "%s" not have const "ROUTE"!', $entityClass);
    //         }
    //         if (!defined("$entityClass::IS_LIST")) {
    //             throw new \Exception('The class "%s" not have const "IS_LIST"!', $entityClass);
    //         }

    //         $slug = $this->slugRepository->findOneByEntity($entityClass::ROUTE, $id, $entityClass::IS_LIST);
    //         $entity->setSlug($slug->getSlug());
    //     }

    //     if ($this->request->isXmlHttpRequest() && $property = $this->request->query->get('property')) {
    //         $newValue = 'true' === mb_strtolower($this->request->query->get('newValue'));
    //         $fieldsMetadata = $this->entity['list']['fields'];

    //         if (!isset($fieldsMetadata[$property]) || 'toggle' !== $fieldsMetadata[$property]['dataType']) {
    //             throw new \RuntimeException(sprintf('The type of the "%s" property is not "toggle".', $property));
    //         }

    //         $this->updateEntityProperty($entity, $property, $newValue);

    //         // cast to integer instead of string to avoid sending empty responses for 'false'
    //         return new Response((int) $newValue);
    //     }

    //     $fields = $this->entity['edit']['fields'];

    //     $editForm = $this->executeDynamicMethod('createEditForm', [$entity, $fields]);
    //     $deleteForm = $this->createDeleteForm($this->entity['name'], $id);

    //     $editForm->handleRequest($this->request); // set entity values from edit form!

    //     if ($editForm->isSubmitted() && $editForm->isValid()) {
    //         $this->processUploadedFiles($editForm);

    //         // save the slug part 2
    //         if (method_exists($entityClass, 'getSlug')) {
    //             $slug->setSlug($entity->getSlug());
    //         }

    //         $this->dispatch(EasyAdminEvents::PRE_UPDATE, ['entity' => $entity]);
    //         $this->executeDynamicMethod('updateEntity', [$entity, $editForm]);
    //         $this->dispatch(EasyAdminEvents::POST_UPDATE, ['entity' => $entity]);

    //         return $this->redirectToReferrer();
    //     }

    //     $this->dispatch(EasyAdminEvents::POST_EDIT);

    //     $parameters = [
    //         'form' => $editForm->createView(),
    //         'entity_fields' => $fields,
    //         'entity' => $entity,
    //         'delete_form' => $deleteForm->createView(),
    //     ];

    //     return $this->executeDynamicMethod('renderTemplate', ['edit', $this->entity['templates']['edit'], $parameters]);
    // }

/*
    protected function deleteAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_DELETE);

        if ('DELETE' !== $this->request->getMethod()) {
            return $this->redirect($this->generateUrl('easyadmin', ['action' => 'list', 'entity' => $this->entity['name']]));
        }

        $id = $this->request->query->get('id');
        $form = $this->createDeleteForm($this->entity['name'], $id);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $easyadmin = $this->request->attributes->get('easyadmin');
            $entity = $easyadmin['item'];

            $this->dispatch(EasyAdminEvents::PRE_REMOVE, ['entity' => $entity]);

            try {
                $this->executeDynamicMethod('removeEntity', [$entity, $form]);
            } catch (ForeignKeyConstraintViolationException $e) {
                throw new EntityRemoveException(['entity_name' => $this->entity['name'], 'message' => $e->getMessage()]);
            }

            $this->dispatch(EasyAdminEvents::POST_REMOVE, ['entity' => $entity]);
        }

        // delete the slug part 1
        $slug = $this->slugRepository->findOneByEntity(Product::ROUTE, $id, Product::IS_LIST);
        if (!is_null($slug)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($slug);
            $em->flush();
        }

        $this->dispatch(EasyAdminEvents::POST_DELETE);

        return $this->redirectToReferrer();
    }
*/
}