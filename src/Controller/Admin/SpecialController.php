<?php

namespace App\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\EasyAdminBatchFormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;


class SpecialController extends EasyAdminController
{
    /**
     * The method that is executed when the user performs a 'list' action on an entity.
     *
     * @return Response
     */
    protected function listAjaxAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $this->entity['list']['dql_filter'] = 'entity.special = 1';
        
        $fields = $this->entity['list']['fields'];

        // TODO: find best way
        unset($fields['enabled']);
        unset($fields['price']);
        unset($fields['publishedAt']);

        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createAjaxBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createAjaxDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->renderTemplate('list', 'admin/default/list.ajax.html.twig', $parameters);
        // return $this->renderTemplate('list', $this->entity['templates']['list'], $parameters);
    }

    /**
     * The method that is executed when the user performs a 'edit' action on an entity.
     *
     * @return Response|RedirectResponse
     *
     * @throws \RuntimeException
     */
    protected function editAction()
    {
        parent::dispatch(EasyAdminEvents::PRE_EDIT);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        if ($this->request->isXmlHttpRequest() && $property = $this->request->query->get('property')) {
            $newValue = 'true' === mb_strtolower($this->request->query->get('newValue'));
            $fieldsMetadata = $this->entity['list']['fields'];

            if (!isset($fieldsMetadata[$property]) || 'toggle' !== $fieldsMetadata[$property]['dataType']) {
                throw new \RuntimeException(sprintf('The type of the "%s" property is not "toggle".', $property));
            }

            parent::updateEntityProperty($entity, $property, $newValue);

            // cast to integer instead of string to avoid sending empty responses for 'false'
            return new Response((int) $newValue);
        }

        $fields = $this->entity['edit']['fields'];

        $editForm = parent::executeDynamicMethod('createEditForm', [$entity, $fields]);

        if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax') {
            $deleteForm = $this->createAjaxDeleteForm($this->entity['name'], $id);
        } else {
            $deleteForm = $this->createDeleteForm($this->entity['name'], $id);
        }

        $editForm->handleRequest($this->request);

        if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax' && $editForm->isSubmitted()) {
            $data_fields = [];
            $data_fields[mb_strtolower($this->entity['name'])] = [];
            $results = ['success' => false, 'fields' => $data_fields];
            $changed = false;
            $editForm2 = $editForm;
            $deleteCheckbox = $this->request->get(mb_strtolower($this->entity['name']))['imageFile']['delete'] ?? null;

            if (isset($fields['imageFile']) && (null !== $editForm->getNormData()->getImageFile() || (null !== $deleteCheckbox))) {
                $changed = true;
            }

                if ($editForm->isValid()) {
                    parent::processUploadedFiles($editForm);

                    parent::dispatch(EasyAdminEvents::PRE_UPDATE, ['entity' => $entity]);
                    parent::executeDynamicMethod('updateEntity', [$entity, $editForm]);
                    parent::dispatch(EasyAdminEvents::POST_UPDATE, ['entity' => $entity]);
                    
                    $editForm2 = parent::executeDynamicMethod('createEditForm', [$entity, $fields]);
                }

            if ($changed) {
                $parameters = [
                    'form' => $editForm2->createView(),
                    'entity_fields' => $fields,
                    'entity' => $entity,
                    '_entity_config' => $easyadmin['entity'],
                ];

                $twig = $this->get('twig');
                $template = $twig->loadTemplate('admin/default/field_image.html.twig');
                $result = $template->renderBlock('entity_image_form', $parameters);

                $data_fields[mb_strtolower($this->entity['name'])][] = [
                    'field_name' => 'imageFile',
                    'html' => $result,
                ];
            }

            // refresh row start
            $fields = $this->entity['list']['fields'];

                // TODO: find best way
                unset($fields['enabled']);
                unset($fields['price']);
                unset($fields['publishedAt']);

            $parameters = [
                'entity' => $entity,
                'entity_name' => $this->entity['name'],
                'fields' => $fields,
                'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
                'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
            ];

            $twig = $this->get('twig');
            $template = $twig->loadTemplate('admin/default/list_item.html.twig');
            $result = $template->renderBlock('table_body', $parameters);

            $data_rows[mb_strtolower($this->entity['name'])][] = [
                'entity_id' => $entity->getId(),
                'html' => $result,
            ];

            $results['success'] = true;
            $results['fields'] = $data_fields;
            $results['rows'] = $data_rows;
            // refresh row end
        
            return new JsonResponse($results);
        }

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            parent::processUploadedFiles($editForm);

            parent::dispatch(EasyAdminEvents::PRE_UPDATE, ['entity' => $entity]);
            parent::executeDynamicMethod('updateEntity', [$entity, $editForm]);
            parent::dispatch(EasyAdminEvents::POST_UPDATE, ['entity' => $entity]);

            return parent::redirectToReferrer();
        }

        parent::dispatch(EasyAdminEvents::POST_EDIT);

        $parameters = [
            'form' => $editForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ];

        if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax') {
            return parent::renderTemplate('edit', 'admin/default/edit.ajax.html.twig', $parameters);
        }

        return parent::renderTemplate('edit', $this->entity['templates']['edit'], $parameters);

        // return $this->executeDynamicMethod('render<EntityName>Template', ['edit', $this->entity['templates']['edit'], $parameters]);
    }

    /**
     * The method that is executed when the user performs a 'new' action on an entity.
     *
     * @return Response|RedirectResponse
     */
    protected function newAction()
    {
        parent::dispatch(EasyAdminEvents::PRE_NEW);

        $entity = parent::executeDynamicMethod('createNewEntity');

        $easyadmin = $this->request->attributes->get('easyadmin');
        $easyadmin['item'] = $entity;
        $this->request->attributes->set('easyadmin', $easyadmin);

        $fields = $this->entity['new']['fields'];

        $newForm = parent::executeDynamicMethod('createNewForm', [$entity, $fields]);

        $newForm->handleRequest($this->request);

        if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax' && $newForm->isSubmitted()) {
            $data_rows = [];
            $data_rows[mb_strtolower($this->entity['name'])] = [];
            // $results = ['success' => false, 'fields' => $data_fields];
            $results = ['success' => false];
            // $changed = false;
            // $newForm2 = $newForm;
            // $deleteCheckbox = $this->request->get(mb_strtolower($this->entity['name']))['imageFile']['delete'] ?? null;

            // if (null !== $newForm->getNormData()->getImageFile() || (null !== $deleteCheckbox)) {
            //     $changed = true;
            // }

            $parameters = [];

            if ($newForm->isValid()) {
                parent::processUploadedFiles($newForm);

                parent::dispatch(EasyAdminEvents::PRE_PERSIST, ['entity' => $entity]);
                parent::executeDynamicMethod('persistEntity', [$entity, $newForm]);
                parent::dispatch(EasyAdminEvents::POST_PERSIST, ['entity' => $entity]);

                $entityRelated = $this->request->query->get('entity_related');
                $idRelated = $this->request->query->get('id_related');

                if ((null !== $entityRelated && !empty($entityRelated)) && (null !== $idRelated && !empty($idRelated)))  {
                    $entityRelated = $this->em->getRepository('\\App\\Entity\\' . $entityRelated)->find($idRelated);
                    
                    if (null !== $entityRelated)  {
                        // // $entityRelated = $this->em->getRepository('\\App\\Entity\\' . $entityRelated)->find($idRelated);
                        // // $entity->addModule($entityRelated);
                        // $entity->setModule($entityRelated);
                        // $this->em->persist($entity);
                        // $this->em->flush();
                    }
                }

                $fields = $this->entity['list']['fields'];

                // TODO: find best way
                unset($fields['enabled']);
                unset($fields['price']);
                unset($fields['publishedAt']);

                $parameters = [
                    'entity' => $entity,
                    'entity_name' => $this->entity['name'],
                    'fields' => $fields,
                    'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
                    'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
                ];

                $twig = $this->get('twig');
                $template = $twig->loadTemplate('admin/default/list_item.html.twig');
                $result = $template->renderBlock('table_body', $parameters);

                $data_rows[mb_strtolower($this->entity['name'])][] = [
                    'html' => $result,
                ];

                $results['success'] = true;
                $results['rows'] = $data_rows;
                
                return new JsonResponse($results);
            }
        }

        if ($newForm->isSubmitted() && $newForm->isValid()) {
            parent::processUploadedFiles($newForm);

            parent::dispatch(EasyAdminEvents::PRE_PERSIST, ['entity' => $entity]);
            parent::executeDynamicMethod('persistEntity', [$entity, $newForm]);
            parent::dispatch(EasyAdminEvents::POST_PERSIST, ['entity' => $entity]);

            return parent::redirectToReferrer();
        }

        parent::dispatch(EasyAdminEvents::POST_NEW, [
            'entity_fields' => $fields,
            'form' => $newForm,
            'entity' => $entity,
        ]);

        $parameters = [
            'form' => $newForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
        ];

        if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax') {
            return parent::executeDynamicMethod('renderTemplate', ['new', 'admin/default/new.ajax.html.twig', $parameters]);
        }

        return parent::executeDynamicMethod('renderTemplate', ['new', $this->entity['templates']['new'], $parameters]);
    }

    /**
     * The method that is executed when the user performs a 'delete' action to
     * remove any entity.
     *
     * @return RedirectResponse
     *
     * @throws EntityRemoveException
     */
    protected function deleteAction()
    {
        parent::dispatch(EasyAdminEvents::PRE_DELETE);

        if ('DELETE' !== $this->request->getMethod()) {
            return $this->redirect($this->generateUrl('easyadmin', ['action' => 'list', 'entity' => $this->entity['name']]));
        }

        $id = $this->request->query->get('id');
        $form = parent::createDeleteForm($this->entity['name'], $id);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $easyadmin = $this->request->attributes->get('easyadmin');
            $entity = $easyadmin['item'];

            parent::dispatch(EasyAdminEvents::PRE_REMOVE, ['entity' => $entity]);

            if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax') {
                $results = ['success' => false];

                try {
                    $deletedId = $entity->getId();
                    parent::executeDynamicMethod('removeEntity', [$entity, $form]);
                    $results['success'] = true;
                    // $results['entity_id'] = $deletedId;
                    $results['entity_ids'] = [$deletedId];
                } catch (ForeignKeyConstraintViolationException $e) {
                    throw new EntityRemoveException(['entity_name' => $this->entity['name'], 'message' => $e->getMessage()]);
                    $results['success'] = false;
                }

                return new JsonResponse($results);
            }

            try {
                parent::executeDynamicMethod('removeEntity', [$entity, $form]);
            } catch (ForeignKeyConstraintViolationException $e) {
                throw new EntityRemoveException(['entity_name' => $this->entity['name'], 'message' => $e->getMessage()]);
            }

            parent::dispatch(EasyAdminEvents::POST_REMOVE, ['entity' => $entity]);
        }

        parent::dispatch(EasyAdminEvents::POST_DELETE);

        return parent::redirectToReferrer();
    }

    /**
     * The method that is executed when the user performs a 'batch' action to any entity.
     */
    protected function batchAction(): Response
    {
        $batchForm = $this->createAjaxBatchForm($this->entity['name']);
        $batchForm->handleRequest($this->request);

        if ($batchForm->isSubmitted() && $batchForm->isValid()) {
            $actionName = $batchForm->get('name')->getData();
            $actionIds = $batchForm->get('ids')->getData();

            $batchActionResult = parent::executeDynamicMethod($actionName.'BatchAction', [$actionIds, $batchForm]);

            if ($batchActionResult instanceof Response) {
                return $batchActionResult;
            }

            if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax') {
                $results = ['success' => false];
                $results['success'] = true;
                $results['entity_ids'] = $actionIds;

                return new JsonResponse($results);
            }
        }

        return $this->redirectToReferrer();
    }

    /**
     * Creates the form used to delete an entity. It must be a form because
     * the deletion of the entity are always performed with the 'DELETE' HTTP method,
     * which requires a form to work in the current browsers.
     *
     * @param string     $entityName
     * @param int|string $entityId   When reusing the delete form for multiple entities, a pattern string is passed instead of an integer
     *
     * @return Form|FormInterface
     */
    protected function createAjaxDeleteForm($entityName, $entityId)
    {
        /** @var FormBuilder $formBuilder */
        $formBuilder = $this->get('form.factory')->createNamedBuilder('delete_form')
            ->setAction($this->generateUrl('easyadmin', ['action' => 'delete', 'entity' => $entityName, 'id' => $entityId, 'editType' => 'ajax']))
            ->setMethod('DELETE')
        ;
        $formBuilder->add('submit', SubmitType::class, ['label' => 'delete_modal.action', 'translation_domain' => 'EasyAdminBundle']);
        // needed to avoid submitting empty delete forms (see issue #1409)
        $formBuilder->add('_easyadmin_delete_flag', HiddenType::class, ['data' => '1']);

        return $formBuilder->getForm();
    }

    protected function createAjaxBatchForm(string $entityName): FormInterface
    {
        return $this->get('form.factory')->createNamed('batch_form', EasyAdminBatchFormType::class, null, [
            'action' => $this->generateUrl('easyadmin', ['action' => 'batch', 'entity' => $entityName, 'editType' => 'ajax']),
            'entity' => $entityName,
        ]);
    }
}