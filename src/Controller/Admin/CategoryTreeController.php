<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\Routing\Annotation\Route;


class CategoryTreeController extends EasyAdminController
{

    /**
     * The method that is executed when the user performs a 'list' action on an entity.
     *
     * @return Response
     */
    protected function listAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        // return $this->executeDynamicMethod('renderTemplate', ['list', $this->entity['templates']['list'], $parameters]);
        return $this->renderTemplate('list', 'admin/category/list.tree.html.twig', $parameters);
    }

    // protected function findAll($entityClass, $page = 1, $maxPerPage = 15, $sortField = null, $sortDirection = null, $dqlFilter = null)
    // {
    //     if (null === $sortDirection || !\in_array(strtoupper($sortDirection), ['ASC', 'DESC'])) {
    //         $sortDirection = 'DESC';
    //     }

    //     $queryBuilder = $this->executeDynamicMethod('createListQueryBuilder', [$entityClass, $sortDirection, $sortField, $dqlFilter]);

    //     $this->filterQueryBuilder($queryBuilder);

    //     $this->dispatch(EasyAdminEvents::POST_LIST_QUERY_BUILDER, [
    //         'query_builder' => $queryBuilder,
    //         'sort_field' => $sortField,
    //         'sort_direction' => $sortDirection,
    //     ]);

    //     return $this->get('easyadmin.paginator')->createOrmPaginator($queryBuilder, $page, $maxPerPage);
    // }

    // Creates the Doctrine query builder used to get all the items. Override it
    // to filter the elements displayed in the listing
    // protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = null);
    public function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = null)
    {
        $classMetadata = $this->em->getClassMetadata($entityClass);

        // $queryBuilder = $this->em->createQueryBuilder()
        //     ->select('entity')
        //     ->from($entityClass, 'entity')
        //     // ->where('entity.parent is NULL')
        //     // ->groupBy("entity.parent")
        //     // ->orderBy("entity.sort", "ASC")
        // ;

        $queryBuilder = $this->em->createQueryBuilder()
            ->select('entity')
            ->from($entityClass, 'entity')
            // ->addOrderBy('entity.sort', 'ASC')
            // ->addOrderBy('entity.lvl', 'ASC')
            // ->groupBy('entity.root')
            // ->where('entity.root = 1')
            // ->orderBy("entity.sort", "ASC")
            // ->getQuery()
        ;

        $isSortedByDoctrineAssociation = $this->isDoctrineAssociation($classMetadata, $sortField);
        if ($isSortedByDoctrineAssociation) {
            $sortFieldParts = explode('.', $sortField);
            $queryBuilder->leftJoin('entity.'.$sortFieldParts[0], $sortFieldParts[0]);
        }

        if (!empty($dqlFilter)) {
            $queryBuilder->andWhere($dqlFilter);
        }

        if (null !== $sortField) {
            if ($sortField == 'id') {
                 $queryBuilder->orderBy('entity.root, entity.lft', $sortDirection);
            }
            $queryBuilder->addOrderBy(sprintf('%s%s', $isSortedByDoctrineAssociation ? '' : 'entity.', $sortField), $sortDirection);
        }

        return $queryBuilder;
    }

    protected function isDoctrineAssociation(ClassMetadata $classMetadata, $fieldName)
    {
        if (null === $fieldName) {
            return false;
        }

        $fieldNameParts = explode('.', $fieldName);

        return false !== strpos($fieldName, '.') && !\array_key_exists($fieldNameParts[0], $classMetadata->embeddedClasses);
    }
}