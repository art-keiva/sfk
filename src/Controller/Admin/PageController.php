<?php

namespace App\Controller\Admin;

use App\Entity\Page;
use App\Repository\ModuleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
// use Symfony\Component\EventDispatcher\GenericEvent;
// use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Annotation\Route;


class PageController extends EasyAdminController
{
    private $moduleRepo;

    public function __construct(ModuleRepository $moduleRepo)
    {
        $this->moduleRepo = $moduleRepo;
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ForbiddenActionException
     */
    public function indexAction(Request $request)
    {
        parent::initialize($request);

        if (null === $request->query->get('entity')) {
            return parent::redirectToBackendHomepage();
        }

        $action = $request->query->get('action', 'list');
        if (!parent::isActionAllowed($action)) {
            throw new ForbiddenActionException(['action' => $action, 'entity_name' => $this->entity['name']]);
        }

        if (\in_array($action, ['show', 'edit', 'new'])) {
            $id = $this->request->query->get('id');
            $entity = $this->request->attributes->get('easyadmin')['item'];
            $requiredPermission = $this->entity[$action]['item_permission'];
            $userHasPermission = $this->get('easyadmin.security.authorization_checker')->isGranted($requiredPermission, $entity);
            if (false === $userHasPermission) {
                throw new NoPermissionException(['action' => $action, 'entity_name' => $this->entity['name'], 'entity_id' => $id]);
            }
        }

        return parent::executeDynamicMethod($action.'Action');
    }

    /**
     * The method that is executed when the user performs a 'edit' action on an entity.
     *
     * @return Response|RedirectResponse
     *
     * @throws \RuntimeException
     */
    protected function editAction()
    {
        // dump($this);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        if ($this->request->isXmlHttpRequest() && $property = $this->request->query->get('property')) {
            $newValue = 'true' === mb_strtolower($this->request->query->get('newValue'));
            $fieldsMetadata = $this->entity['list']['fields'];

            if (!isset($fieldsMetadata[$property]) || 'toggle' !== $fieldsMetadata[$property]['dataType']) {
                throw new \RuntimeException(sprintf('The type of the "%s" property is not "toggle".', $property));
            }

            $this->updateEntityProperty($entity, $property, $newValue);

            // cast to integer instead of string to avoid sending empty responses for 'false'
            return new Response((int) $newValue);
        }

        $fields = $this->entity['edit']['fields'];

        $editForm = parent::createEditForm($entity, $fields);
        
        $controllerList = [];

        foreach (self::getControllers() as $key => $value) {
            $controllerList[$value] = $key;
        }

        $editForm->add('controller', ChoiceType::class, [
            'choices' => $controllerList,
            'placeholder' => false,
            'required'   => false,
            'empty_data'  => '',
        ]);

        $deleteForm = parent::createDeleteForm($this->entity['name'], $id);

        $editForm->handleRequest($this->request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            parent::processUploadedFiles($editForm);

            $submittedController = $editForm->getNormData()->getController();
            if (!empty($submittedController)) {
                $findedEntity = $this->em->getRepository(Page::class)->findOneBy(['controller' => $submittedController]);
                if (null !== $findedEntity && $findedEntity !== $entity) {
                    $findedEntity->setController('');
                }
            }

            parent::dispatch(EasyAdminEvents::PRE_UPDATE, ['entity' => $entity]);
            parent::updateEntity($entity, $editForm);
            parent::dispatch(EasyAdminEvents::POST_UPDATE, ['entity' => $entity]);

            return parent::redirectToReferrer();
        }

        $parameters = [
            'form' => $editForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'pageModules' => $this->moduleRepo->findPageModules($entity),
            'activeModules' => $this->moduleRepo->findPageModules(),
            'modules' => $this->getModules(),
        ];

        // dump($parameters);

        return parent::renderTemplate('edit', 'admin/page/edit.html.twig', $parameters);

        // return $this->render('admin/page/edit.html.twig', [
        //     $parameters,
        // ]);
    }

    /**
     * The method that is executed when the user performs a 'new' action on an entity.
     *
     * @return Response|RedirectResponse
     */
    protected function newAction()
    {
        parent::dispatch(EasyAdminEvents::PRE_NEW);

        $entity = parent::executeDynamicMethod('createNewEntity');

        $easyadmin = $this->request->attributes->get('easyadmin');
        $easyadmin['item'] = $entity;
        $this->request->attributes->set('easyadmin', $easyadmin);

        $fields = $this->entity['new']['fields'];

        $newForm = parent::executeDynamicMethod('createNewForm', [$entity, $fields]);

        $newForm->handleRequest($this->request);
        if ($newForm->isSubmitted() && $newForm->isValid()) {
            parent::processUploadedFiles($newForm);

            parent::dispatch(EasyAdminEvents::PRE_PERSIST, ['entity' => $entity]);
            parent::executeDynamicMethod('persistEntity', [$entity, $newForm]);
            parent::dispatch(EasyAdminEvents::POST_PERSIST, ['entity' => $entity]);

            return parent::redirectToReferrer();
        }

        parent::dispatch(EasyAdminEvents::POST_NEW, [
            'entity_fields' => $fields,
            'form' => $newForm,
            'entity' => $entity,
        ]);

        $parameters = [
            'form' => $newForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
        ];

        return parent::executeDynamicMethod('renderTemplate', ['new', $this->entity['templates']['new'], $parameters]);
    }

    /**
     * The method that is executed when the user performs a 'delete' action to
     * remove any entity.
     *
     * @return RedirectResponse
     *
     * @throws EntityRemoveException
     */
    protected function deleteAction()
    {
        parent::dispatch(EasyAdminEvents::PRE_DELETE);

        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        // prevent delete home page
        if (null !== $entity->getSlug() && '' === $entity->getSlug()) {
            $this->addFlash('danger', 'Forbidden to delete the main page.');
            return parent::redirectToReferrer();
        }

        if ('DELETE' !== $this->request->getMethod()) {
            return $this->redirect($this->generateUrl('easyadmin', ['action' => 'list', 'entity' => $this->entity['name']]));
        }

        $id = $this->request->query->get('id');
        $form = parent::createDeleteForm($this->entity['name'], $id);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $easyadmin = $this->request->attributes->get('easyadmin');
            $entity = $easyadmin['item'];

            parent::dispatch(EasyAdminEvents::PRE_REMOVE, ['entity' => $entity]);

            try {
                parent::executeDynamicMethod('removeEntity', [$entity, $form]);
            } catch (ForeignKeyConstraintViolationException $e) {
                throw new EntityRemoveException(['entity_name' => $this->entity['name'], 'message' => $e->getMessage()]);
            }

            parent::dispatch(EasyAdminEvents::POST_REMOVE, ['entity' => $entity]);
        }

        parent::dispatch(EasyAdminEvents::POST_DELETE);

        return parent::redirectToReferrer();
    }

    private function getModules()
    {
        return [
            'textblock' => 'label.module.textblock',
            'infoblock' => 'label.module.infoblock',
            'fileblock' => 'label.module.fileblock',
            'slider' => 'label.module.slider',
            'contact' => 'label.module.contact',
            'category' => 'label.module.category',
            'product' => 'label.module.product',
            'service' => 'label.module.service',
            'action' => 'label.module.action',
            'specialist' => 'label.module.specialist',
            'gallery' => 'label.module.gallery',
            'faq' => 'label.module.faq',
            'testimonial' => 'label.module.testimonial',
            'blog' => 'label.module.blog',
        ];
    }

    public static function getControllers()
    {
        return [
            '' => 'label.empty_value',
            'about' => 'label.controller.about',
            'service' => 'label.controller.service',
            'catalog' => 'label.controller.catalog',
            'action' => 'label.controller.action',
            'specialist' => 'label.controller.specialist',
            'blog' => 'label.controller.blog',
            'contact' => 'label.controller.contact',
            'faq' => 'label.controller.faq',
            'gallery' => 'label.controller.gallery',
            'testimonial' => 'label.controller.testimonial',
            'price' => 'label.controller.price',
            'terms' => 'label.controller.terms',
        ];
    }
}