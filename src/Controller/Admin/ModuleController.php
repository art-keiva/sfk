<?php

namespace App\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Finder\Finder;
// use Symfony\Component\EventDispatcher\GenericEvent;
// use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;


class ModuleController extends EasyAdminController
{

    private $translator;

    public function __construct(TranslatorInterface $translator) {
        $this->translator = $translator;
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ForbiddenActionException
     */
    public function indexAction(Request $request)
    {
        parent::initialize($request);

        if (null === $request->query->get('entity')) {
            return parent::redirectToBackendHomepage();
        }

        $action = $request->query->get('action', 'list');
        if (!parent::isActionAllowed($action)) {
            throw new ForbiddenActionException(['action' => $action, 'entity_name' => $this->entity['name']]);
        }

        if (\in_array($action, ['show', 'edit', 'new'])) {
            $id = $this->request->query->get('id');
            $entity = $this->request->attributes->get('easyadmin')['item'];
            $requiredPermission = $this->entity[$action]['item_permission'];
            $userHasPermission = $this->get('easyadmin.security.authorization_checker')->isGranted($requiredPermission, $entity);
            if (false === $userHasPermission) {
                throw new NoPermissionException(['action' => $action, 'entity_name' => $this->entity['name'], 'entity_id' => $id]);
            }
        }

        return parent::executeDynamicMethod($action.'Action');
    }

    /**
     * The method that is executed when the user performs a 'edit' action on an entity.
     *
     * @return Response|RedirectResponse
     *
     * @throws \RuntimeException
     */
    protected function editAction()
    {
        parent::dispatch(EasyAdminEvents::PRE_EDIT);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        // $this->addFlash('info', 'Test flash message!');

        if ($this->request->isXmlHttpRequest() && $property = $this->request->query->get('property')) {
            $newValue = 'true' === mb_strtolower($this->request->query->get('newValue'));
            $fieldsMetadata = $this->entity['list']['fields'];

            if (!isset($fieldsMetadata[$property]) || 'toggle' !== $fieldsMetadata[$property]['dataType']) {
                throw new \RuntimeException(sprintf('The type of the "%s" property is not "toggle".', $property));
            }

            parent::updateEntityProperty($entity, $property, $newValue);

            // cast to integer instead of string to avoid sending empty responses for 'false'
            return new Response((int) $newValue);
        }

        $fields = $this->entity['edit']['fields'];

        $editForm = parent::executeDynamicMethod('createEditForm', [$entity, $fields]);
        $deleteForm = parent::createDeleteForm($this->entity['name'], $id);

        // $editForm->add('position', ChoiceType::class, [
        //     'choices' => ['label.top' => 0, 'label.right' => 1, 'label.bottom' => 2, 'label.left' => 3],
        // ]);
        
        // template field  dynamicly add choices
        if (!empty($entity->getName())) {
            $templateList = [];
            $finder = new Finder();
            $finder->in($this->getParameter('kernel.project_dir') . '/templates/module/' . $entity->getName());

            if ($finder->hasResults()) {
                foreach ($finder as $file) {
                    $parts = explode('.', $file->getRelativePathname());
                    $first = array_shift($parts);
                    $templateList[(string) $this->translator->trans('label.template.' . $first, [], 'admin')] = (string) $first;
                }

                // $templateField = $editForm->get('template');
                
                $editForm->add('template', ChoiceType::class, [
                    'choices' => $templateList,
                    'preferred_choices' => ['default'],
                    'placeholder' => false,
                    'required'   => false,
                    'empty_data'  => '',
                ]);
            }
        }

        $editForm->handleRequest($this->request);

        if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax' && $editForm->isSubmitted()) {

            $entityRelated = $this->request->query->get('entity_related');
            $idRelated = $this->request->query->get('id_related');

            $entityRelated = $this->em->getRepository('\\App\\Entity\\' . $entityRelated)->find($idRelated);

            if (null !== $entityRelated) {
                $entity->addPage($entityRelated);
                $this->em->persist($entity);
                $this->em->flush();
            }
            
            // $data_fields = [];
            // $data_fields[mb_strtolower($this->entity['name'])] = [];
            // $results = ['success' => false, 'fields' => $data_fields];
            // $changedImage = false;
            // $changedBackground = false;
            // $editForm2 = $editForm;
            // $deleteImage = $this->request->get(mb_strtolower($this->entity['name']))['imageFile']['delete'] ?? null;
            // $deleteBackground = $this->request->get(mb_strtolower($this->entity['name']))['backgroundFile']['delete'] ?? null;

            // if (null !== $editForm->getNormData()->getImageFile() || (null !== $deleteImage)) {
            //     $changedImage = true;
            // }

            // if (null !== $editForm->getNormData()->getBackgroundFile() || (null !== $deleteBackground)) {
            //     $changedBackground = true;
            // }

            // if ($changedImage || $changedBackground) {
            //     if ($editForm->isValid()) {
            //         parent::processUploadedFiles($editForm);

            //         parent::dispatch(EasyAdminEvents::PRE_UPDATE, ['entity' => $entity]);
            //         parent::executeDynamicMethod('updateEntity', [$entity, $editForm]);
            //         parent::dispatch(EasyAdminEvents::POST_UPDATE, ['entity' => $entity]);
                    
            //         $editForm2 = parent::executeDynamicMethod('createEditForm', [$entity, $fields]);
            //     }

            //     $parameters = [
            //         'form' => $editForm2->createView(),
            //         'entity_fields' => $fields,
            //         'entity' => $entity,
            //         '_entity_config' => $easyadmin['entity'],
            //     ];

            //     $twig = $this->get('twig');
            //     $template = $twig->loadTemplate('admin/default/field_image.html.twig');

            //     if ($changedImage) {
            //         $result = $template->renderBlock('entity_image_form', $parameters);

            //         $data_fields[mb_strtolower($this->entity['name'])][] = [
            //             'field_name' => 'imageFile',
            //             'html' => $result,
            //         ];
            //     }
            //     if ($changedBackground) {
            //         $result = $template->renderBlock('entity_background_form', $parameters);

            //         $data_fields[mb_strtolower($this->entity['name'])][] = [
            //             'field_name' => 'backgroundFile',
            //             'html' => $result,
            //         ];
            //     }

            //     $results['success'] = true;
            //     $results['fields'] = $data_fields;
                
            //     return new JsonResponse($results);
            // }
        }

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            parent::processUploadedFiles($editForm);

            parent::dispatch(EasyAdminEvents::PRE_UPDATE, ['entity' => $entity]);
            parent::executeDynamicMethod('updateEntity', [$entity, $editForm]);
            parent::dispatch(EasyAdminEvents::POST_UPDATE, ['entity' => $entity]);

            return parent::redirectToReferrer();
        }

        parent::dispatch(EasyAdminEvents::POST_EDIT);

        $parameters = [
            'form' => $editForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ];

        if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax') {
            return parent::renderTemplate('edit', 'admin/module/edit.ajax.html.twig', $parameters);
        }

        // return parent::renderTemplate('edit', 'admin/module/edit.html.twig', $parameters);
        return parent::renderTemplate('edit', $this->entity['templates']['edit'], $parameters);

        // return $this->executeDynamicMethod('render<EntityName>Template', ['edit', $this->entity['templates']['edit'], $parameters]);
    }

    /**
     * The method that is executed when the user performs a 'new' action on an entity.
     *
     * @return Response|RedirectResponse
     */
    protected function newAction()
    {
        parent::dispatch(EasyAdminEvents::PRE_NEW);

        $entity = parent::executeDynamicMethod('createNewEntity');

        $easyadmin = $this->request->attributes->get('easyadmin');
        $easyadmin['item'] = $entity;
        $this->request->attributes->set('easyadmin', $easyadmin);

        $fields = $this->entity['new']['fields'];

        $newForm = parent::executeDynamicMethod('createNewForm', [$entity, $fields]);

        // template field  dynamicly add choices
        if (!empty($this->request->query->get('entity_field_name'))) {
            $templateList = [];
            $finder = new Finder();
            $finder->in($this->getParameter('kernel.project_dir') . '/templates/module/' . $this->request->query->get('entity_field_name'));
            // $finder->in($this->getParameter('kernel.project_dir') . '/templates/module/infoblock');

            if ($finder->hasResults()) {
                foreach ($finder as $file) {
                    $parts = explode('.', $file->getRelativePathname());
                    $first = array_shift($parts);
                    $templateList[(string) 'label.template.' . $first] = (string) $first;
                }

                // $templateField = $newForm->get('template');
                // dump($templateField);
                $newForm->add('template', ChoiceType::class, [
                    'choices' => $templateList,
                    'preferred_choices' => ['default'],
                    'placeholder' => false,
                    'required'   => false,
                    'empty_data'  => '',
                ]);
            }
        }
// dump($templateList);
        // dump($newForm);
        // $newForm->get('template')
        //     ->addModelTransformer(new CallbackTransformer(
        //         function ($tagsAsArray) {
        //             // transform the array to a string
        //             return implode(', ', $templateList);
        //         },
        //         function ($tagsAsString) {
        //             // transform the string back to an array
        //             return explode(', ', $templateList);
        //         }
        //     ))
        // ;
// dump($newForm);
        // if (!$newForm->isSubmitted()) {
        // }

        $newForm->handleRequest($this->request);


        if ($newForm->isSubmitted() && $newForm->isValid()) {
            parent::processUploadedFiles($newForm);

            parent::dispatch(EasyAdminEvents::PRE_PERSIST, ['entity' => $entity]);
            parent::executeDynamicMethod('persistEntity', [$entity, $newForm]);
            parent::dispatch(EasyAdminEvents::POST_PERSIST, ['entity' => $entity]);

            if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax') {
                $results = ['success' => false];

                $entityRelated = $this->request->query->get('entity_related');
                $idRelated = $this->request->query->get('id_related');

                $entityRelated = $this->em->getRepository('\\App\\Entity\\' . $entityRelated)->find($idRelated);

                if (null !== $entityRelated) {
                    $entity->addPage($entityRelated);
                    $this->em->persist($entity);
                    $this->em->flush();

                    $this->request->query->set('action', 'edit');
                    $this->request->query->set('id', $entity->getId());
                    // $this->request->query->set('entity_related', $this->request->query->get('entity_related'));
                    // $this->request->query->set('id_related', $this->request->query->get('id_related'));
                    // $this->request->query->set('editType', $this->request->query->get('editType'));

                    // return $this->redirectToRoute('easyadmin', $this->request->query->all());
                    // $twig = $this->get('twig');
                    // $template = $twig->loadTemplate('admin/default/list_item.html.twig');
                    // $result = $template->renderBlock('table_body', $parameters);

                    $fields = $this->entity['edit']['fields'];

                    $editForm = parent::executeDynamicMethod('createEditForm', [$entity, $fields]);
                    $deleteForm = parent::createDeleteForm($this->entity['name'], $entity->getId());

                    $parameters = [
                        'form' => $editForm->createView(),
                        'entity_fields' => $fields,
                        'entity' => $entity,
                        'delete_form' => $deleteForm->createView(),
                    ];

                    $html = parent::renderTemplate('edit', 'admin/module/edit.ajax.html.twig', $parameters);

                    $data_tab = [
                        'entity_id' => $entity->getId(),
                        'html' => $html,
                    ];

                    $results['success'] = true;
                    $results['tab'] = $data_tab;

                    return new JsonResponse($results);
                }

            }

            return parent::redirectToReferrer();
        }

        parent::dispatch(EasyAdminEvents::POST_NEW, [
            'entity_fields' => $fields,
            'form' => $newForm,
            'entity' => $entity,
        ]);

        $parameters = [
            'form' => $newForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
        ];

        if (null !== $this->request->query->get('editType') && $this->request->query->get('editType') === 'ajax') {
            return parent::executeDynamicMethod('renderTemplate', ['new', 'admin/module/new.ajax.html.twig', $parameters]);
        }

        return parent::executeDynamicMethod('renderTemplate', ['new', $this->entity['templates']['new'], $parameters]);
    }

    /**
     * @return JsonResponse
     */
    protected function unlinkAction()
    {
        $results = ['success' => false];

        $easyadmin = $this->request->attributes->get('easyadmin');

        $entity = $easyadmin['item'];
        $id = $this->request->query->get('id');

        $entityRelated = $this->request->query->get('entity_related');
        $idRelated = $this->request->query->get('id_related');

        $entityRelated = $this->em->getRepository(\App\Entity\Page::class)->find($idRelated);

        if (null !== $entityRelated) {
            $entity->removePage($entityRelated);
            $this->em->persist($entity);
            $this->em->flush();

            $results['success'] = true;
        }

        return new JsonResponse($results);
    }
}