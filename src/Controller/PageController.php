<?php

namespace App\Controller;

use App\Repository\PageRepository as Pages;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\Urlizer;

class PageController extends AbstractController
{
    public function list(Request $request, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');
        $stackSlugs = [];

        if (null !== $slugRouter->landingPage) {
            $stackSlugs[] = $slugRouter->landingPage->getSlug();
            $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));
        }

        return $this->render('page/list.html.twig', [
            'page' 		 	=> $slugRouter->landingPage,
            'slug_router' 	=> $slugRouter,
        ]);
    }

    public function nav($slug_router, string $position = '', Pages $pages)
    {
        $template = 'default/_nav.html.twig';

        if (!empty($position)) {
           $template = 'default/_nav.' . $position . '.html.twig';
        }

        return $this->render($template, [
            'pages' => $pages->findBy(['enabled' => true], ['sort' => 'ASC']),
            'slug_router' => $slug_router,
        ]);
    }

    public function footerLinks(Pages $pages)
    {
        return $this->render('default/_footer_links.html.twig', [
            'pages' => $pages->findBy(['enabled' => true], ['sort' => 'ASC']),
        ]);
    }
}
