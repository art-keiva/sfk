<?php

namespace App\Controller;

use App\Repository\PageRepository as Pages;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\Urlizer;
use App\Utils\Info;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\NamedAddress;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class ContactController extends AbstractController
{
    public function list(Request $request, Breadcrumbs $breadcrumbs, Urlizer $url, Info $info, Pages $pages, MailerInterface $mailer): Response
    {
        $slugRouter = $request->get('slug_router');
        $stackSlugs = [];

        if (null !== $slugRouter->landingPage) {
            $stackSlugs[] = $slugRouter->landingPage->getSlug();
            $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));
        }

        // feedback form
        $formBuilder = $this->createFormBuilder([], [
            'csrf_protection' => true,
            // 'allow_extra_fields' => true,
        ]);

        $formBuilder
            ->add('credential', TextType::class, [
                'label' => 'label.feedback.credential',
                'data_class' => null,
                'mapped' => false,
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.feedback.email',
                'data_class' => null,
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email([
                        // 'message' => 'The email "{{ value }}" is not a valid email.',
                        // 'message' => 'error.valid.email',
                    ]),
                ],
            ])
            ->add('message', TextAreaType::class, [
                'label' => 'label.feedback.message',
                'data_class' => null,
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 10,
                        'max' => 65535,
                    ]),
                ],
                'invalid_message' => 'error.valid.message',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'label.send'
            ])
        ;

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailData = (new TemplatedEmail())
                ->from(new NamedAddress($this->getParameter('mailer.name'), $info->project['sitename']))
                // ->to('company@gmail.com')
                ->to(new Address($info->project['email']))
                //->cc('cc@example.com')
                //->bcc('bcc@example.com')
                //->replyTo('fabien@example.com')
                //->priority(Email::PRIORITY_HIGH)
                ->subject('Обратная связь, от ' . $form->get('email')->getData())
                // ->text('Sending emails is fun again!')
                // ->html($emailContent)
                ->htmlTemplate('emails/contact.feedback.html.twig')
                ->context([
                    'userCredential' => $form->get('credential')->getData(),
                    'userEmail' => $form->get('email')->getData(),
                    'userMessage' => $form->get('message')->getData(),
                ])
            ;

            try {
                $mailer->send($emailData);
            } catch (Exception $e) {
                throw new \Exception('Failed to send message.');
            }

            $this->addFlash('success', 'label.mailer.success');

            return $this->redirect($request->getUri());
        }

        return $this->render('contact/list.html.twig', [
            'page'          => $slugRouter->landingPage,
            'slug_router'   => $slugRouter,
            'module_args'   => [
                'form'          => $form->createView(),
                'page_terms'    => $pages->findOneBy(['controller' => 'terms']),
            ],
        ]);
    }
}
