<?php

namespace App\Controller;

use App\Repository\GtagRepository as Categories;
use App\Repository\GalleryRepository as Galleries;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\Urlizer;

class GalleryController extends AbstractController
{
    public function list(Request $request, Categories $categories, Galleries $galleries, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');
        $stackSlugs = [];

        if (null !== $slugRouter->landingPage) {
            $stackSlugs[] = $slugRouter->landingPage->getSlug();
            $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));
        }

        return $this->render('gallery/list.html.twig', [
            'page'          => $slugRouter->landingPage,
            'slug_router'   => $slugRouter,
            'categories'    => $categories->findBy([], ['sort' => 'ASC', 'id' => 'DESC']),
            'galleries'     => $galleries->findBy([], ['sort' => 'ASC', 'createdAt' => 'DESC']),
        ]);
    }
}
