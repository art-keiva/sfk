<?php

namespace App\Controller;

use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\Urlizer;

class TermsController extends AbstractController
{
    public function list(Request $request, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');
        $stackSlugs = [];

        if (null !== $slugRouter->landingPage) {
            $stackSlugs[] = $slugRouter->landingPage->getSlug();
            $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));
        }

        return $this->render('terms/list.html.twig', [
            'page'          => $slugRouter->landingPage,
            'slug_router'   => $slugRouter,
        ]);
    }
}
