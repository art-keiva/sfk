<?php

namespace App\Controller;

use App\Entity\Page;
use App\Repository\ActionRepository as Actions;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\Urlizer;

class ActionController extends AbstractController
{

    public function list(Request $request, Actions $actions, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');
        $stackSlugs = [];

        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }
        }

        $request->query = $request->attributes->get('query') ?? $request->query;

        return $this->render('action/list.html.twig', [
            'page'              => $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'paginator'         => $actions->findLatest($request->query->get('page') ?? 1),
            'items' => $actions->findBy(['enabled' => true], ['sort' => 'ASC', 'createdAt' => 'DESC']),
        ]);
    }

    public function show(Request $request, Actions $actions, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');

        $item = $actions->find($slugRouter->findedSlug->getEntityId());
        
        $stackSlugs = [];
        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }
        }

        $stackSlugs[] = $item->getSlug();
        $breadcrumbs->addItem($item->getTitle(), $url->url($stackSlugs));

        return $this->render('action/show.html.twig', [
            'page'              => $item ?? $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'item'              => $item,
            'items' => $actions->findBy(['enabled' => true], ['sort' => 'ASC', 'createdAt' => 'DESC']),
        ]);
    }
}
