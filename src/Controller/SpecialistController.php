<?php

namespace App\Controller;

use App\Entity\Page;
use App\Repository\SpcategoryRepository as Categories;
use App\Repository\SpecialistRepository as Specialists;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\Urlizer;

class SpecialistController extends AbstractController
{

    public function list(Request $request, Categories $categories, Specialists $specialists, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');
        $category = null;
        $stackSlugs = [];

        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }

            $category = $categories->findOneById($value->getEntityId());

            if (null !== $category) {
                $stackSlugs[] = $category->getSlug();
                $breadcrumbs->addItem($category->getTitle(), $url->url($stackSlugs));
            }
        }

        $request->query = $request->attributes->get('query') ?? $request->query;

        return $this->render('specialist/list.html.twig', [
            'page'              => $category ?? $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'categories'        => $categories->getChildren($category, true, 'sort'),
            'paginator'         => $specialists->findLatest($request->query->get('page') ?? 1, $category),
        ]);
    }

    public function show(Request $request, Categories $categories, Specialists $specialists, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');

        $item = $specialists->find($slugRouter->findedSlug->getEntityId());
        
        $stackSlugs = [];
        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }
            
            if ($value->getIsList()) {
                $category = $categories->findOneById($value->getEntityId());

                if (null !== $category) {
                    $stackSlugs[] = $category->getSlug();
                    $breadcrumbs->addItem($category->getTitle(), $url->url($stackSlugs));
                }
            }
        }

        $stackSlugs[] = $item->getSlug();
        $breadcrumbs->addItem($item->getTitle(), $url->url($stackSlugs));

        return $this->render('specialist/show.html.twig', [
            'page'              => $item ?? $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'item'              => $item,
        ]);
    }
}
