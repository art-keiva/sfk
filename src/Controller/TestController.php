<?php

namespace App\Controller;

use App\Service\SlugRouter;
use App\Service\DataFixtures;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TestController extends AbstractController
{
    public function list(Request $request, SlugRouter $slugRouter, DataFixtures $fixtures): Response
    {

        $data = [];

        // $data[] = $fixtures->getRandomText();
        // $data[] = $fixtures->getRandomTags();
        // $data[] = $fixtures->getRandomLink();
        // $data[] = $fixtures->getRandomTag();
        // $data[] = $fixtures->getRandomImage();
        // $data[] = $fixtures->getRandomImages();
        // $data[] = $fixtures->getRandomParagraph();
        // $data[] = $fixtures->getRandomParagraphs(random_int(4, 10));

        // $i = 1;
        // while ($i <= 10) {
        //     $res = $i % 2;
        //     $data[] = settype($res, 'bool');
        //     $data[] = $res;
        //     $i++;
        // }

        return $this->render('default/test.html.twig', [
            'slug_router'   => $slugRouter,
            'data'   => $data,
        ]);
    }
}
