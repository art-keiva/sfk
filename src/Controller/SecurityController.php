<?php

namespace App\Controller;

use App\Service\SlugRouter;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils, SlugRouter $slugRouter, Breadcrumbs $breadcrumbs, TranslatorInterface $translator): Response
    {
        // if ($this->getUser()) {
        //    $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $breadcrumbs->addItem($translator->trans('trans.page.login.title'), 'login');

        return $this->render('security/login.html.twig', [
            'page' => [
                'title' => $translator->trans('trans.page.login.title'),
                'metaTitle' => $translator->trans('trans.page.login.title'),
                'metaDescription' => '',
                'metaKeyword' => '',
            ],
            'last_username' => $lastUsername,
            'error' => $error,
            'slug_router' => $slugRouter,
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
}
