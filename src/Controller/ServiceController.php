<?php

namespace App\Controller;

use App\Entity\Page;
use App\Repository\ScategoryRepository as Categories;
use App\Repository\ServiceRepository as Services;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\Urlizer;

class ServiceController extends AbstractController
{

    public function list(Request $request, Categories $categories, Services $services, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');
        $category = null;
        $stackSlugs = [];

        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }

            $category = $categories->findOneById($value->getEntityId());

            if (null !== $category) {
                $stackSlugs[] = $category->getSlug();
                $breadcrumbs->addItem($category->getTitle(), $url->url($stackSlugs));
            }
        }

        $request->query = $request->attributes->get('query') ?? $request->query;

        return $this->render('service/list.html.twig', [
            'page'              => $category ?? $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'categories'        => $categories->getChildren($category, true, 'sort'),
            'paginator'         => $services->findLatest($request->query->get('page') ?? 1, $category),
        ]);
    }

    public function show(Request $request, Categories $categories, Services $services, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');

        $item = $services->find($slugRouter->findedSlug->getEntityId());
        
        $stackSlugs = [];
        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }
            
            if ($value->getIsList()) {
                $category = $categories->findOneById($value->getEntityId());

                if (null !== $category) {
                    $stackSlugs[] = $category->getSlug();
                    $breadcrumbs->addItem($category->getTitle(), $url->url($stackSlugs));
                }
            }
        }

        $stackSlugs[] = $item->getSlug();
        $breadcrumbs->addItem($item->getTitle(), $url->url($stackSlugs));

        return $this->render('service/show.html.twig', [
            'page'              => $item ?? $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'item'              => $item,
        ]);
    }
}
