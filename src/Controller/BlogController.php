<?php

namespace App\Controller;

use App\Entity\Page;
use App\Repository\BcategoryRepository as Categories;
use App\Repository\BlogRepository as Blogs;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\Urlizer;

class BlogController extends AbstractController
{

    public function list(Request $request, Categories $categories, Blogs $blogs, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');
        $category = null;
        $stackSlugs = [];

        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }

            $category = $categories->findOneById($value->getEntityId());

            if (null !== $category) {
                $stackSlugs[] = $category->getSlug();
                $breadcrumbs->addItem($category->getTitle(), $url->url($stackSlugs));
            }
        }

        $request->query = $request->attributes->get('query') ?? $request->query;

        return $this->render('blog/list.html.twig', [
            'page'              => $category ?? $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'categories'        => $categories->getChildren($category, true, 'sort'),
            'paginator'         => $blogs->findLatest($request->query->get('page') ?? 1, $category),
        ]);
    }

    public function show(Request $request, Categories $categories, Blogs $blogs, Breadcrumbs $breadcrumbs, Urlizer $url): Response
    {
        $slugRouter = $request->get('slug_router');

        $item = $blogs->find($slugRouter->findedSlug->getEntityId());
        
        $stackSlugs = [];
        foreach ($slugRouter->items as $key => $value) {
            if ($value->getRoute() === Page::ROUTE && null !== $slugRouter->landingPage) {

                $stackSlugs[] = $slugRouter->landingPage->getSlug();
                $breadcrumbs->addItem($slugRouter->landingPage->getTitle(), $url->url($stackSlugs));

                continue;
            }
            
            if ($value->getIsList()) {
                $category = $categories->findOneById($value->getEntityId());

                if (null !== $category) {
                    $stackSlugs[] = $category->getSlug();
                    $breadcrumbs->addItem($category->getTitle(), $url->url($stackSlugs));
                }
            }
        }

        $stackSlugs[] = $item->getSlug();
        $breadcrumbs->addItem($item->getTitle(), $url->url($stackSlugs));

        return $this->render('blog/show.html.twig', [
            'page'              => $item ?? $slugRouter->landingPage,
            'slug_router'       => $slugRouter,
            'item'              => $item,
        ]);
    }

    public function latest(Request $request, Blogs $blogs): Response
    {
        return $this->render('blog/latest.html.twig', [
            'items'         => $blogs->findBy(['enabled' => true], ['publishedAt' => 'DESC'], 3, 0),
        ]);
    }
}
