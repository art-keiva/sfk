const jQueryBridget = require('jquery-bridget');
const Isotope = require('isotope-layout');
const imagesLoaded = require('imagesloaded');

require('isotope-cells-by-row');

jQueryBridget( 'isotope', Isotope, $ );
imagesLoaded.makeJQueryPlugin( $ );