import '../scss/app.scss';
import '@fancyapps/fancybox/dist/jquery.fancybox.min.css';


// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');

// create global $ and jQuery variables
global.$ = global.jQuery = $;

require('bootstrap');

require("@fancyapps/fancybox");



function autoHeight(items_class)
{
    var maxHeight = 0;

    $(items_class).each(function(){
      if ( $(this).height() > maxHeight ) 
      {
        maxHeight = $(this).height();
      }
    });
     
    $(items_class).height(maxHeight);
}

global.autoHeight = autoHeight;


$(function(){

	
	// navbar dropdown on hover
	$('.navbar .nav-item.dropdown.dropdown-hover').on({
		mouseenter: function () {
			// $(this).dropdown('show');
			$(this).children('.dropdown-toggle').attr('aria-expanded', 'true');
			$(this).children('.dropdown-menu').addClass('show');
		},
		mouseleave: function () {
			// $(this).dropdown('hide');
			$(this).children('.dropdown-toggle').attr('aria-expanded', 'false');
			$(this).children('.dropdown-menu').removeClass('show');
		}
	});

	// tree sidebar nav toggle icon
	$('.tree [data-toggle=\'collapse\']').on('click', function() {
		var $icon = $(this).find('i');
		if ($icon.hasClass('fa-plus')) {
			$icon.removeClass('fa-plus').addClass('fa-minus');
		} else {
			$icon.removeClass('fa-minus').addClass('fa-plus');
		}
	});

	// accordion toggle icon
	$('.accordion .card-header .btn').on('click', function() {
		var $parent = $(this).closest('.accordion');
		$parent.find('.card-header .btn i').removeClass('fa-angle-up').addClass('fa-angle-down');

		var $icon = $(this).find('i');
		if ($icon.hasClass('fa-angle-down') && ($(this).hasClass('collapsed') === false)) {
			$icon.removeClass('fa-angle-up').addClass('fa-angle-down');
		} else {
			$icon.removeClass('fa-angle-down').addClass('fa-angle-up');
		}
	});
	
});