import '../../node_modules/photoswipe/src/css/main.scss';
import '../../node_modules/photoswipe/src/css/default-skin/default-skin.scss';
// import '../../node_modules/photoswipe/dist/photoswipe.css';
// import '../../node_modules/photoswipe/dist/default-skin/default-skin.css';

const PhotoSwipe = require('photoswipe');
const PhotoSwipeUI_Default = require('photoswipe/dist/photoswipe-ui-default');

global.PhotoSwipe = PhotoSwipe;
global.PhotoSwipeUI_Default = PhotoSwipeUI_Default;