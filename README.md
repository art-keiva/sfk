# SFK #
notes:
[
    C:\Server\domains\sfk\vendor\easycorp\easyadmin-bundle\src\Controller\AdminControllerTrait.php
    C:\Server\domains\sfk\vendor\easycorp\easyadmin-bundle\src\Search\Autocomplete.php
    $this->getParameter('kernel.project_dir')
    C:\Server\domains\sfk\vendor\friendsofsymfony\ckeditor-bundle\src\Config\CKEditorConfiguration.php
]

..

admin:
	user:
		form:
			+ password: encode/decode
	product:
		form:
			+ image: find file system and download them
            + add createAction with Slug
			+ price: change int to decimal
			+ enabled: toggle instead checkbox
			+ description,summary: find wysiwyg
            + get slug data in product entity
	pcategory:
		form:
			+ parent (@Gedmo\TreeParent)
            + change editAction with Slug
            + add createAction with Slug
    page:
        + handle page entity
    
    entities:
        + add updatedAt, createdAt in all emntities
        + change upload image, timestamps, enabled and other field names
    image:
        - unique name generation system for all upload images

    + breadcrumbs - add filters for list view breadcrumb-item
    + additional tabs
    + top actions - module ajax: fix top actions
    - list view
    + testimonial - add enabled
    + faq - add special on the tag form
    + checkbox - more size
    + delimeter on the faq edit form, between tag and image collection
    ~ module ajax - fix batch delete form


db:
	+ migrate to the mysql, slug table contsraint unique rows
	pcategory, product_pcategory:
		+ use orm many to many, don't use own logic!
	slug:
		+ route logic based on opencart

site:
	+ canonical
    + design that, short global informations
    page:
        + set meta data in all pages
        + in twig include additional blocks like heading_title
        + to develop breadcrumb or find best solution
	slug:
		+ `slug` route set pagination
		+ route links for `slug` router
        + event subscribers: change slug getters in entities via es
	product:
		show:
			+ image, price, description
    home:
        + design to infoblocks
    module:
        tree:
            + active category

debug:
	+ clear the mess: create first service for slug variables
    - optimizing $this->em->getRepository($class) => $this->em->getRepository('\\App\\Entity\\' . $entityRelated)
	- entity slug create em query instead OneToOne
	+ fix product link in category page on the slug route
	- persist slug in product and category entities via entity listener https://stackoverflow.com/questions/32197296/symfony-get-entity-from-within-another-entity
    - change slug entity, better use number marks instead string in field route
    - liip image filters: fix png quality, rename file if filename is not latin
    - vich uploader: rename cyrillic filenames to latin [vendor\vich\uploader-bundle\Util\Transliterator]
    - breadcrumbs change simple markup to scheme.org
    - optimizing ajax add/edit, template field [src\Controller\Admin\ModuleController.php]
    - add new field 'module' for determine entities and link them for page, for full changeability of seo pages [src\Service\SlugRouter.php controllerDeterminant()]
    - fix seo url paths without logic sequence
    - fix src\Controller\Module\TextblockController.php repo is not found
    - fix src\Entity\Fileblock.php allow only office docs

----------------------------------------------------------------------------------------------------------
# Symfony console commands #

php bin/console 

Available commands:
    about                                   Displays information about the current project
    help                                    Displays help for a command
    list                                    Lists commands

assets:
    assets:install                          Installs bundles web assets under a public directory

cache:
    cache:clear                             Clears the cache
    cache:pool:clear                        Clears cache pools
    cache:pool:delete                       Deletes an item from a cache pool
    cache:pool:list                         List available cache pools
    cache:pool:prune                        Prunes cache pools
    cache:warmup                            Warms up an empty cache

config:
    config:dump-reference                   Dumps the default configuration for an extension debug
    debug:autowiring                        Lists classes/interfaces you can use for autowiring
    debug:config                            Dumps the current configuration for an extension
    debug:container                         Displays current services for an application
    debug:event-dispatcher                  Displays configured listeners for an application
    debug:form                              Displays form type information
    debug:router                            Displays current routes for an application
    debug:translation                       Displays translation messages information
    debug:twig                              Shows a list of twig functions, filters, globals and tests

doctrine:
    doctrine:cache:clear-collection-region  Clear a second-level cache collection region
    doctrine:cache:clear-entity-region      Clear a second-level cache entity region
    doctrine:cache:clear-metadata           Clears all metadata cache for an entity manager
    doctrine:cache:clear-query              Clears all query cache for an entity manager
    doctrine:cache:clear-query-region       Clear a second-level cache query region
    doctrine:cache:clear-result             Clears result cache for an entity manager
    doctrine:cache:contains                 Check if a cache entry exists
    doctrine:cache:delete                   Delete a cache entry
    doctrine:cache:flush                    [doctrine:cache:clear] Flush a given cache
    doctrine:cache:stats                    Get stats on a given cache provider
    doctrine:database:create                Creates the configured database
    doctrine:database:drop                  Drops the configured database
    doctrine:database:import                Import SQL file(s) directly to Database.
    doctrine:ensure-production-settings     Verify that Doctrine is properly configured for a production environment
    doctrine:fixtures:load                  Load data fixtures to your database
    doctrine:generate:entities              [generate:doctrine:entities] Generates entity classes and method stubs from your mapping information
    doctrine:mapping:convert                [orm:convert:mapping] Convert mapping information between supported formats
    doctrine:mapping:import                 Imports mapping information from an existing database
    doctrine:mapping:info
    doctrine:migrations:diff                [diff] Generate a migration by comparing your current database to your mapping information.
    doctrine:migrations:dump-schema         [dump-schema] Dump the schema for your database to a migration.
    doctrine:migrations:execute             [execute] Execute a single migration version up or down manually.
    doctrine:migrations:generate            [generate] Generate a blank migration class.
    doctrine:migrations:latest              [latest] Outputs the latest version number
    doctrine:migrations:migrate             [migrate] Execute a migration to a specified version or the latest available version.
    doctrine:migrations:rollup              [rollup] Rollup migrations by deleting all tracked versions and insert the one version that exists.
    doctrine:migrations:status              [status] View the status of a set of migrations.
    doctrine:migrations:up-to-date          [up-to-date] Tells you if your schema is up-to-date.
    doctrine:migrations:version             [version] Manually add and delete migration versions from the version table.
    doctrine:query:dql                      Executes arbitrary DQL directly from the command line
    doctrine:query:sql                      Executes arbitrary SQL directly from the command line.
    doctrine:schema:create                  Executes (or dumps) the SQL needed to generate the database schema
    doctrine:schema:drop                    Executes (or dumps) the SQL needed to drop the current database schema
    doctrine:schema:update                  Executes (or dumps) the SQL needed to update the database schema to match the current mapping metadata
    doctrine:schema:validate                Validate the mapping files

lint:
    lint:twig                               Lints a template and outputs encountered errors
    lint:xliff                              Lints a XLIFF file and outputs encountered errors
    lint:yaml                               Lints a file and outputs encountered errors

make:
    make:auth                               Creates a Guard authenticator of different flavors
    make:command                            Creates a new console command class
    make:controller                         Creates a new controller class
    make:crud                               Creates CRUD for Doctrine entity class
    make:entity                             Creates or updates a Doctrine entity class, and optionally an API Platform resource
    make:fixtures                           Creates a new class to load Doctrine fixtures
    make:form                               Creates a new form class
    make:functional-test                    Creates a new functional test class
    make:migration                          Creates a new migration based on database changes
    make:registration-form                  Creates a new registration form system
    make:serializer:encoder                 Creates a new serializer encoder class
    make:serializer:normalizer              Creates a new serializer normalizer class
    make:subscriber                         Creates a new event subscriber class
    make:twig-extension                     Creates a new Twig extension class
    make:unit-test                          Creates a new unit test class
    make:user                               Creates a new security user class
    make:validator                          Creates a new validator and constraint class
    make:voter                              Creates a new security voter class

router:
    router:match                            Helps debug routes by simulating a path info match

security:
    security:encode-password                Encodes a password.

server:
    server:dump                             Starts a dump server that collects and displays dumps in a single place

translation:
    translation:update                      Updates the translation file

----------------------------------------------------------------------------------------------------------

    php bin/console cache:clear
    
[validate entity mapping]
    php bin/console doctrine:schema:validate
    php bin/console doctrine:schema:drop --force
    php bin/console doctrine:schema:update --force
    php bin/console doctrine:database:drop --force
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:diff
    php bin/console doctrine:migrations:migrate
    php bin/console doctrine:schema:validate
    php bin/console doctrine:fixtures:load
    
    php bin/console doctrine:schema:drop --force
    php bin/console doctrine:database:drop --force
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:diff
    php bin/console doctrine:migrations:migrate
    php bin/console doctrine:fixtures:load

[liip imagine]
    php bin/console liip:imagine:cache:remove
    php bin/console liip:imagine:cache:remove --filters=product
    php bin/console liip:imagine:cache:resolve media/nikon_d300_5.jpg --filters=product

[translation]
    php bin/console translation:update ru --force

----------------------------------------------------------------------------------------------------------
# Modules logic #

page
+-----------+-----------+-----------+---------+
| id        | route     | title     | enabled |
+-----------+-----------+-----------+---------+
| 1         | index     | Главная   | 1       |
| 2         | about     | О нас     | 1       |
| 3         | product   | Каталог   | 1       |
+-----------+-----------+-----------+---------+

module
+-----------+-----------+-----------+-----------+----------+----------+------+---------+
| id        | name      | title     | image     | template | position | sort | enabled |
+-----------+-----------+-----------+-----------+----------+----------+------+---------+
| 56        | slider    | Слайдер   |           |          | top      | 1    | 1       |
| 57        | slider    | Слайдер   |           |          | top      | 1    | 1       |
| 58        | gallery   | Галерея   |           |          | top      | 3    | 1       |
| 59        | service   | Услуги    |           |          | bottom   | 2    | 1       |
+-----------+-----------+-----------+-----------+----------+----------+------+---------+

page_module
+-----------+-----------+
| page_id   | module_id |
+-----------+-----------+
| 1         | 56        |
| 2         | 57        |
| 1         | 58        |
| 2         | 57        |
| 2         | 58        |
+-----------+-----------+

slider
+-----------+-----------+-----------------+------+---------+
| id        | title     | image           | sort | enabled |
+-----------+-----------+-----------------+------+---------+
| 11        | Слайд 1   | macbook.jpg     | 1    | 1       |
| 12        | Слайд 2   | iphone.jpg      | 2    | 1       |
| 13        | Слайд 3   | sony_vaio_5.jpg | 3    | 1       |
+-----------+-----------+-----------------+------+---------+

module_slider
+-----------+-----------+
| module_id | slider_id |
+-----------+-----------+
| 56        | 11        |
| 56        | 12        |
| 57        | 13        |
+-----------+-----------+

gallery
+-----------+-------------------+----------------+------------------+------+
| id        | gallery_tab_id    | title          | image            | sort |
+-----------+-------------------+----------------+------------------+------+
| 32        | 24                | Лечение 1      | macbook.jpg      | 0    |
| 33        | 24                | Лечение 2      | iphone.jpg       | 0    |
| 34        | 25                | Имплантация 1  | sony_vaio_5.jpg  | 0    |
| 35        | 23                | Диагностика 1  | nikon_d300_5.jpg | 0    |
| 36        | 0                 | Протезирование | samsungtab.jpg   | 0    |
+-----------+-------------------+----------------+------------------+------+

gallery_tab
+-----------+-------------+------+---------+
| id        | title       | sort | enabled |
+-----------+-------------+------+---------+
| 23        | Диагностика | 1    | 1       |
| 24        | Лечение     | 2    | 1       |
| 25        | Имплантация | 3    | 1       |
+-----------+-------------+------+---------+

таблица module - code, settings, title, description, image, enabled
таблица page_module - page_id, module_id, position, sort
вся логика в ModuleController, вызов с твига с передачей page_id


----------------------------------------------------------------------------------------------------------

faq
+-----------+-------------------+----------------+------------------+------+------+
| id        | fcategory_id      | question       | answer           | home | sort |
+-----------+-------------------+----------------+------------------+------+------+
| 32        | 24                | Лечение 1      | macbook.jpg      | 0    | 0    |
| 33        | 24                | Лечение 2      | iphone.jpg       | 1    | 0    |
| 34        | 25                | Имплантация 1  | sony_vaio_5.jpg  | 0    | 0    |
| 35        | 23                | Диагностика 1  | nikon_d300_5.jpg | 1    | 0    |
| 36        | 0                 | Протезирование | samsungtab.jpg   | 0    | 0    |
+-----------+-------------------+----------------+------------------+------+------+

fcategory
+-----------+-------------+------+---------+
| id        | title       | sort | enabled |
+-----------+-------------+------+---------+
| 23        | Диагностика | 1    | 1       |
| 24        | Лечение     | 2    | 1       |
| 25        | Имплантация | 3    | 1       |
+-----------+-------------+------+---------+

----------------------------------------------------------------------------------------------------------

entity special

+src\Controller\Admin\PageController.php
+config\packages\admin\entities\Service.yaml
+src\Entity\Service.php
+src\DataFixtures\ServiceFixtures.php
+src\Controller\Admin\ServiceController.php
+src\Controller\Module\ServiceController.php
+templates\module\service\default.html.twig

----------------------------------------------------------------------------------------------------------

git:

    clone
        git clone https://art-keiva@bitbucket.org/art-keiva/sfk.git .

    checkout
        git checkout master
        git pull

    tags
        git fetch --all --tags --prune

    steps
        git fetch --all --tags --prune
        git checkout master
        git pull
            [if not]
                git stash (optional)
                git config --global user.name "cd94006" (optional)
                git checkout master
                git pull
        php bin/console cache:clear

composer:

    ? composer create-project vendor/project